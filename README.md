# MotionTestSignals

Test signals and analysis code for motion base testing. 

## Purpose

The notebooks and code herein can do the following:

- Generate test signals for a Stewart platform motion base

  * Motion base performance testing (speed, linearity, frequency response)
  
  * Weight and center of gravity identification
  
  * Moment of inertia identification
  
## Software needed

The notebooks and supporting code run with python. An
Anaconda/Miniconda installation with some additions should be sufficient. 

	conda install h5py
	

## Use

Run the notebooks by running `jupyter-lab` in the `notebooks` folder

## Installation

To keep the notebooks 'clean', the pre-commit script clears the
notebooks (remove plot results and the like.

  
## Authors

Rene van Paassen <m.m.vanpaassen@tudelft.nl>
