#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 15:24:51 2022

@author: repa
"""

import numpy as np
import quaternion
from motiongen import JlimMotionProperty, JlimMotionSolution, \
    getConstantSines, getFadeInFadeOut
from BTrees.OOBTree import OOBTree
from matplotlib import pyplot as plt
from sympy import Symbol, Piecewise, integrate, lambdify
from jlimits import JLAVTrans

_eps = 1e-11

def dprint(*args, **kwargs):
    print(*args, **kwargs)
    pass

def rodriguez(q):
        # remember: w:9, x:10, y:11, z:12
        return np.array((
            np.arctan2(2.0*(q.y*q.z+q.x*q.w),
                       q.w*q.w-q.x*q.x-q.y*q.y+q.z*q.z),
            np.arcsin(-2.0*(q.x*q.z-q.y*q.w)),
            np.arctan2(2.0*(q.x*q.y+q.w*q.z),
                       q.w*q.w+q.x*q.x-q.y*q.y-q.z*q.z)))

#def qmult(q1, q2):
##    return (q1[0]*q2[0]-q1[1]*q2[1]-q1[2]*q2[2]-q1[3]*q2[3],
#            q1[0]*q2[1]+q1[1]*q2[0]+q1[2]*q2[3]-q1[3]*q2[2],
#            q1[0]*q2[2]+q1[2]*q2[0]+q1[3]*q2[1]-q1[1]*q2[3],
#            q1[0]*q2[3]+q1[3]*q2[0]+q1[1]*q2[2]-q1[2]*q2[1])

class Pose:
    def __init__(self, *args, **kwargs):
        """
        Define a platform/device position

        Parameters, call option vector
        ----------
        x : iterable of number
            13-element vector, uvw, pqr, xyz, attitude quaternion wxyz.

        Parameters, call option position vector + euler angles
        ----------
        x : iterable of number
            3-element vector xyz
        euler: iterable of number
            3-element vector phi, theta, psi

        Parameters, call option position vector + quaternion elements
        ----------
        x : iterable of number
            3-element vector xyz
        quat: iterable of number
            4-element vector w, x, y, z

        Returns
        -------
        None.

        """

        # states:
        # uvw / linear speeds
        # pqr / omega
        # xyz
        # quat
        if len(args) == 1 and len(args[0]) == 13:
            self.x = args[0]
        if len(args) >= 1 and len(args[0]) == 3:
            self.x = np.zeros((13,))
            self.x[6:9] = np.array(args[0])
            if len(args) >= 2:
                if isinstance(args[1], quaternion.quaternion):
                    self.x[9:] = args[1].components
                elif len(args[1]) == 3:
                    # phi, theta, psi
                    cf, sf = np.cos(0.5*args[1][0]), np.sin(0.5*args[1][0])
                    ct, st = np.cos(0.5*args[1][1]), np.sin(0.5*args[1][1])
                    cp, sp = np.cos(0.5*args[1][2]), np.sin(0.5*args[1][2])
                    self.x[9] = cf*ct*cp + sf*st*sp
                    self.x[10] = sf*ct*cp - cf*st*sp
                    self.x[11] = cf*st*cp + sf*ct*sp
                    self.x[12] = cf*ct*sp - sf*st*cp
                    # self.x[9:] = quaternion.from_euler_angles(args[1]).components
                elif len(args[1]) == 4:
                    self.x[9:] = np.quaternion(*args[1]).components
                else:
                    self.x[9:] = np.quaternion(args[1]).components
            else:
                self.x[9] = 1

    def quat(self):
        return quaternion.quaternion(*self.x[9:])

    def xyz(self):
        return self.x[6:9]

    def __add__(self, b):
        return Pose(self.xyz() + b.xyz(), b.quat() * self.quat())

    def __sub__(self, b):
        return Pose(self.xyz() - b.xyz(),
                    self.quat() * b.quat().conjugate() )

    def __repr__(self):
        return f"Pose({self.xyz()}, {self.quat()})"

    def __str__(self):
        return f"Pose({self.xyz()}, {self.quat()})"

    def __mul__(self, factor):
        return Pose(factor*self.xyz(),
                    self.quat()**factor)

    def __rmul__(self, factor):
        return self.__mul__(factor)

    def angle(self):
        """
        Give the rotation angle of this pose

        Returns
        -------
        float
            Angle of rotation, radians.

        """
        return float(2*np.arccos(self.x[9]))

    def axis(self):
        """
        Return the rotation axis of this pose

        Returns
        -------
        np.array((3,), dtype=np.float64)
            Normalized rotation axis.

        """
        n = np.linalg.norm(self.x[10:])
        if n > 1e-15:
            return self.x[10:] / n
        return np.zeros((3,))


    def direction(self):
        """
        Movement direction of the linear translation

        Returns
        -------
        np.array((3,), dtype=np.float64)
            Normalized movement direction

        """
        n = np.linalg.norm(self.x[6:9])
        if n > 1e-15:
            return self.x[10:] / n
        return np.zeros((3,))

    
    def euler(self):
        # remember: w:9, x:10, y:11, z:12
        return np.array((
            np.arctan2(2*(self.x[11]*self.x[12]+self.x[10]*self.x[9]),
                       self.x[9]*self.x[9] -
                       self.x[10]*self.x[10] -
                       self.x[11]*self.x[11] +
                       self.x[12]*self.x[12]),
            np.arcsin(-2.0*(self.x[10]*self.x[12]-self.x[11]*self.x[9])),
            np.arctan2(2.0*(self.x[10]*self.x[11]+self.x[9]*self.x[12]),
                       self.x[9]*self.x[9]+
                       self.x[10]*self.x[10]-
                       self.x[11]*self.x[11]-
                       self.x[12]*self.x[12])))
    def json(self):
        return { 'type': 'Pose', 'uvw': self.x[:3].tolist(),
                 'pqr': self.x[3:6].tolist(),
                 'xyz': self.xyz().tolist(),
                 'phithetapsi': self.euler().tolist()}

    def R(self):
        """Obtain rotation matrix corresponding to the pose attitude

        Returns:
            3x3 np array, corresponding to quaternion as rotation
        """
        return np.array(quaternion.as_rotation_matrix(quaternion.quaternion(*self.x[9:])))

_t = Symbol('t')

class Profile:

    def __init__(self, dist, vmx, amx, jmx):
        """
        Motion profile generator

        Produces a jerk, acceleration and velocity-limited motion profile
        over a distance dist.

        Parameters
        ----------
        dist : float
            Motion distance.
        vmx : float
            Maximum velocity.
        amx : float
            Maximum acceleration.
        jmx : float
            Maximum jerk.

        Raises
        ------
        ValueError
            When the distance is too small for a motion that reaches maximum
            jerk/acceleration. Lower the maximum acceleration

        Returns
        -------
        None.

        """
        self.dist, self.vmx, self.amx, self.jmx = (
            float(dist), float(vmx), float(amx), float(jmx) )
        tj = amx / jmx
        ta = (vmx - tj*amx) / amx
        amn = Piecewise(
            (_t*jmx, _t < tj),
            (amx, _t < tj + ta),
            (-(_t - 2*tj - ta)*jmx, _t < 2*tj+ta),
            (-(_t-(2*tj+ta))*jmx, _t < 3*tj+ta),
            (-amx, _t < 3*tj+2*ta),
            ((_t-(4*tj+2*ta))*jmx, _t >= 3*tj+2*ta))
        dmin = integrate(integrate(amn), (_t, 0, 4*tj+2*ta))
        tv = (dist - dmin) / vmx
        if tv < 0.0:
            raise ValueError(f"Profile negative coast time {tv}")
        self.dt = 4*tj + 2*ta + tv

        self.a = Piecewise(
            (_t*jmx, _t < tj),
            (amx, _t < tj + ta),
            (-(_t - 2*tj - ta)*jmx, _t < 2*tj+ta),
            (0.0, _t < 2*tj+ta+tv),
            (-(_t-(2*tj+ta+tv))*jmx, _t < 3*tj+ta+tv),
            (-amx, _t < 3*tj+2*ta+tv),
            ((_t-(4*tj+2*ta+tv))*jmx, _t >= 3*tj+2*ta+tv))
        self.v = integrate(self.a, _t)
        self.x = integrate(self.v, _t)

        self.accel = lambdify((_t,), self.a)
        self.accel.__doc__ = """
        Return acceleration profile.

        Parameters
        ----------
        t : float
            Time.

        Returns
        -------
        float.

        """
        self.accel.__name__ = 'accel'
        self.vel = lambdify((_t,), self.v)
        self.vel.__doc__ = """
        Returns velocity profile.

        Parameters
        ----------
        t : float
            Time.

        Returns
        -------
        float.

        """
        self.vel.__name__ = 'vel'

        self.pos = lambdify((_t,), self.x)
        """
        Returns position profile.

        Parameters
        ----------
        t : float
            Time.

        Returns
        -------
        float.

        """
        self.pos.__name__ = 'pos'

    def json(self):
        return { 'type': 'Profile', 'dist': self.dist,
                 'vmx': self.vmx, 'amx': self.amx, 'jmx': self.jmx}

class JLimMotionParts:

    def __init__(self, dist, vmx=0.1, amx=1.0, jmx=1.0, leader=None,
                 tcoast=0.0, dt=0.0):

        if leader is not None:
            # follow times and acceleration of leader, scale with distance
            # ratio
            self.dt0a, self.dtaa, self.dt1a, \
                self.dt0b, self.dtab, self.dt1b = \
                    leader.dt0a, leader.dtaa, leader.dt1a, \
                    leader.dt0b, leader.dtab, leader.dt1b
            self.ai = leader.ai * dist/leader.dist
            return

        if dist < 1e-12:
            # no distance, optionally coast
            self.dt0a, self.dtaa, self.dt1a, \
                self.dt0b, self.dtab, self.dt1b = (0.0, ) * 6
            if dt > 0.0 and tcoast % dt > _eps:
                self.dtc = round(tcoast / dt) * dt
            else:
                self.dtc = tcoast

            self.jmx = jmx
            self.ai = 0.0
            return

        # jerk-limited acceleration, to vmx
        #(dt0a, dtaa, dt1a), (dx0, dxa, dx1), ai = \
        #    JlimMotionProperty(0.0, 0.0, vmx, 0.0, amx, jmx)
        if dt > 0.0:
            dt0a, j0, dx0, dtaa, ai, dxa, dt1a, j1, dx1 = JLAVTrans(
                0.0, 0.0, 0.0, vmx, amx, jmx, dt)
            ai = np.abs(ai)
        else:
            (dt0a, dtaa, dt1a), (dx0, dxa, dx1), ai = \
                JlimMotionProperty(0.0, 0.0, vmx, 0.0, amx, jmx)

        # store the resulting times
        self.dt0a, self.dtaa, self.dt1a = dt0a, dtaa, dt1a

        # distance traveled in acceleration phase
        dacc = dx0 + dxa + dx1
        self.dists = np.zeros(7,)
        self.dists[:3] = (dx0, dxa, dx1)

        # braking
        if dt > 0.0:
            self.dt0b, j0, dx0, self.dtab, ai, dxa, self.dt1b, j1, dx1 = \
                JLAVTrans(0.0, vmx, 0.0, 0.0, amx, jmx, dt)
            jmx = 0.5*(np.abs(j0) + np.abs(j1))
            ai = np.abs(ai)
        else:
            (self.dt0b, self.dtab, self.dt1b), (dx0, dxa, dx1), ai = \
                JlimMotionProperty(vmx, 0.0, 0.0, 0.0, amx, jmx)

        ddec = dx0 + dxa + dx1
        self.dists[4:] = (dx0, dxa, dx1)

        # coasting
        if dist - dacc - ddec > _eps:
            dtc = (dist - dacc - ddec) / vmx

            # need to adjust again?
            if dt > 0.0 and np.abs(dtc/dt - np.round(dtc/dt)) > _eps:
                dprint("need to adjust accel phase for coasting speed")
                ddelta = (np.ceil(dtc/dt) - dtc/dt)*dt*vmx
                frac = dist/(dist + ddelta)
                ai = frac*ai
                jmx = frac*jmx
                vmx = frac*vmx
                self.dtc = np.ceil(dtc/dt)*dt
                self.dists = frac*self.dists
            else:
                self.dtc = dtc

        elif dist - dacc - ddec > -_eps:
            self.dtc = 0.0
        else:
            raise ValueError("lower the max speed, trajectory too large")

        self.dists[3] = dtc*vmx
        self.ai = ai
        self.vmx = vmx
        self.jmx = jmx

    def dt(self):
        return self.dt0a + self.dtaa + self.dt1a + \
            self.dt0b + self.dtab + self.dt1b + self.dtc

    def dist(self):
        return JlimMotionSolution(
            self.dt0a, self.dtaa, self.dt1a,
            0.0, 0.0, 0.0, 0.0, self.ai, self.jmx)[1]

'''
    def createMotions(self, x0:Pose, x1:Pose):

        # tree with time (float)/motion data
        motions = OOBTree()

        dist = self.dist()

        # jerk limited acceleration, need to calculate fraction of motion path
        move = x1 - x0
        if self.dt0a > 0.0:
            d1 = 1/6*(self.ai/self.dt0a)**3
            motions[0.0] = Motion(0.0, 0.0, self.ai / self.dt0a,
                                  x0, x0 + move*(d1/dist), self.dt0a)
        if self.dtaa > 0.0:
            # constant acceleration
            motions[self.dt0a] = Motion(self.ai, 0.0)
            d2 = d1 + 0.5*self.ai**2

        # jerk limited decel to 0
        if self.dt0b > 0.0:
            motions[self.dt0a + self.dtaa] = \
                Motion(self.ai, -self.ai / self.dt0b)

        if self.dtc > 0.0:
            # add coasting (at max velocity)
            motions[self.dt0a + self.dtaa + self.dt0b] = \
                Motion(0.0, 0.0)

        # jerk limited deceleration
        if self.dt1a > 0.0:
            motions[self.dt0a + self.dtaa + self.dt0b + self.dtc] = \
                Motion(0.0, -(self.ai / self.dt1a))
        if self.dtab > 0:
            motions[self.dt0a + self.dtaa + self.dt0b + self.dtc +
                    self.dt1a] = \
                Motion(-self.ai, 0.0)
        if self.dt1b > 0.0:
            motions[self.dt0a + self.dtaa + self.dt0b + self.dtc +
                    self.dt1a + self.dtab] = \
                Motion(-self.ai, self.ai / self.dt1b)
        return motions
'''

class Still:

    def __init__(self, x0: Pose, dt):
        """
        Keep the simulator at the current position for some time

        Parameters
        ----------
        dt : float
            Duration of the continous position.

        Returns
        -------
        None.

        """
        self.dt = dt
        self.x0 = x0

    def duration(self):
        return self.dt

    def accel(self, t):
        return np.zeros((6, ))

    def derivative(self, x, t):
        return np.zeros((13, ))

    def velocity(self, t):
        return np.zeros((6,))

    def position(self, t):
        return self.x0

    def json(self):
        return { 'type': 'Still',
                 'xyz': self.x0.xyz().tolist(),
                 'phithetapsi': self.x0.euler().tolist() }

class Movement:

    def __init__(self, x0: Pose, x1: Pose, vmx, amx, jmx, rfactor, dt=0.0):
        """
        velocity, acceleration and jerk-limited movement from x0 to x1

        Parameters
        ----------
        x0 : Pose
            Initial pose for the simulator, position and angles.
        x1 : Pose
            Final pose.
        vmx : float
            Maximum velocity during the movement.
        amx : float
            Maximum acceleration during the movement.
        jmx : float
            Maximum jerk.
        rfactor : float
            Scale factor between linear velocity/acceleration/jerk maxima,
            and rotary maxima.
        dt : float, default 0.0
            Time step of a possible later discretization, used to round off
            different constant acceleration/jerk segments

        Returns
        -------
        None.

        """

        # calculate the linear and angle distance between the two poses
        self.move = x1 - x0
        self.x0 = x0
        dl = float(np.linalg.norm(self.move.xyz()))
        dr = self.move.angle()
        dprint("linear", dl, "angle", dr)

        # linear and or rotational motion
        self.axis = np.zeros((6,))
        if dl > 1e-12:
            self.axis[:3] = self.move.xyz() / dl # normalized
        if dr > 1e-12:
            self.axis[3:] = x0.R().T @ self.move.axis()
            # normalized and converted to inertial
        elif dr < -1e-12:
            self.axis[3:] = -x0.R().T @ self.move.axis() 
            # normalized and converted to inertial

        # time needed movement linear/rotation
        lmove = JLimMotionParts(dl, vmx, amx, jmx, dt=dt)
        rmove = JLimMotionParts(dr, vmx*rfactor, amx*rfactor, jmx*rfactor,
                                dt=dt)

        if lmove.dt() > rmove.dt():
            # linear movement leading, further scale the axis
            #rmove = JLimMotionParts(dr, leader=lmove)
            #self.axis[:3] = self.axis[:3]
            self.axis[3:] = dr / dl * self.axis[3:]
            self.dt = lmove.dt()

            # normalized profile
            self.profile = Profile(dl, lmove.vmx, np.abs(lmove.ai), lmove.jmx)
            self.range = dl
            if np.abs(lmove.dt() - self.profile.dt) > 1e-5:
                raise ValueError("motionparts and profile times differ!")

        else:
            # rotation leading, scale translation
            self.axis[:3] = dl / dr * self.axis[:3]
            #self.axis[3:] = self.axis[3:]
            self.dt = rmove.dt()
            #lmove = JLimMotionParts(dl, leader=rmove)
            self.profile = Profile(dr, rmove.vmx, np.abs(rmove.ai), rmove.jmx)
            self.range = dr
            if np.abs(rmove.dt() - self.profile.dt) > 1e-5:
                raise ValueError("motionparts and profile times differ!")

    def duration(self):
        return self.dt

    def accel(self, t):

        return self.profile.accel(t)*self.axis

    def velocity(self, t):

        return self.profile.vel(t)*self.axis

    def derivative(self, x, t):

        res = np.zeros((13, ))
        res[:6] = self.accel(t)   # deriv linear speed tensor in inertial
                                  # deriv angular vel UG wrt I, 
        res[6:9] = x[:3]          # deriv linear position in I
        W = quaternion.quaternion(0, *x[3:6]) # q for current ang vel wrt I
        q = quaternion.quaternion(*x[9:])     # for current orientation wrt I
        # dqdw = 0.5 * W * q  
        dqdw = 0.5 * q * W        # this seems the wrong one?
        # per https://www.euclideanspace.com/physics/kinematics/angularvelocity/QuaternionDifferentiation2.pdf
        # but it does work correctly.....
        res[9:] = dqdw.components # deriv current orientation
        return res

    def position(self, t):

        dx = self.profile.pos(t)
        return self.x0 + (dx/self.range)*self.move

    def json(self):
        return { 'type': 'Movement',
                 'from': self.x0.json(), 'move': self.move.json(),
                 'axis': self.axis.tolist(), 'profile': self.profile.json(),
                 'dt': self.dt }


class FadedSineProfile:

    def __init__(self, Tfade, Ttotal, omg, gains=None, phi0=None):
        """Create the (1d) profile for a faded sine motion. 

        Arguments:
            Tfade -- Fade-in and fade-out times, [s]
            Ttotal -- Total duration, must be >= 2*Tfade
            omg -- Frequency vector of sine components
            gains -- Gains for the different sine components
            phi0 -- Phase vector of sine starting phases

        Raises:
            ValueError: when the fade-in and total times don't match
            ValueError: when the frequency and phase vectors don't match
        """

        if Ttotal < 2*Tfade:
            raise ValueError("FadedSineProfile, incorrect times")
        self.Ttotal = Ttotal
        self.Tfade = Tfade
        self.omg = np.array(omg)
        if gains is None:
            self.gains = np.ones(self.omg.shape)
        else:
            self.gains = np.array(gains)
        if phi0 is None:
            self.phi0 = np.zeros(self.omg.shape)
        self.phi0 = np.array(phi0)
        if len(self.omg.shape) != 1 or self.omg.shape != self.phi0.shape or \
            self.gains.shape != self.omg.shape:
            raise ValueError("FadedSineProfile, incorrect omg, gains, or phi0")
        
        # functions for fade-in, constant and fade-out sine motion
        fsinyi, fsindyi, fsinddyi, \
            fsinyo, fsindyo, fsinddyo = getFadeInFadeOut()
        fsiny, fsindy, fsinddy = getConstantSines()

        self.mfuns = [
            dict(y=fsinyi, dy=fsindyi, ddy=fsinddyi),
            dict(y=fsiny, dy=fsindy, ddy=fsinddy),
            dict(y=fsinyo, dy=fsindyo, ddy=fsinddyo)
        ]

    def accel(self, t):
        if t < self.Tfade:
            return np.dot(self.gains,
                self.mfuns[0]['ddy'](self.omg, t, self.phi0, self.Tfade))
        elif t > self.Ttotal - self.Tfade:
            return np.dot(self.gains,
                self.mfuns[2]['ddy'](self.omg, t, self.phi0, self.Tfade, self.Ttotal))
        else:
            return np.dot(self.gains,
                self.mfuns[1]['ddy'](self.omg, t, self.phi0, self.Tfade))

    def vel(self, t):
        if t < self.Tfade:
            return np.dot(self.gains,
                self.mfuns[0]['dy'](self.omg, t, self.phi0, self.Tfade))
        elif t > self.Ttotal - self.Tfade:
            return np.dot(self.gains,
                self.mfuns[2]['dy'](self.omg, t, self.phi0, self.Tfade, self.Ttotal))
        else:
            return np.dot(self.gains,
                self.mfuns[1]['dy'](self.omg, t, self.phi0, self.Tfade))

    def pos(self, t):
        if t < self.Tfade:
            return np.dot(self.gains,
                self.mfuns[0]['y'](self.omg, t, self.phi0, self.Tfade))
        elif t > self.Ttotal - self.Tfade:
            return np.dot(self.gains,
                self.mfuns[2]['y'](self.omg, t, self.phi0, self.Tfade, self.Ttotal))
        else:
            return np.dot(self.gains,
                self.mfuns[1]['y'](self.omg, t, self.phi0, self.Tfade))

    def dt(self):
        return self.Ttotal

    def json(self):
        return {'type': 'FadedSineProfile', 
            'Tfade': self.Tfade,
            'Ttotal': self.Ttotal, 
            'omg': self.omg.tolist(),
            'gain': self.gains.tolist(), 
            'phi0': self.phi0.tolist()}


class ConstantAccelProfile:

    def __init__(self, a0, v0, a1, v1, a_tgt, jmx, dt):
        """Profile definition for a constant acceleration, jerk-limited 
            motion from one velocity to another
            
        Arguments:
            a0 -- Initial acceleration at profile start
            v0 -- Initial velocity at profile start
            a1 -- Target acceleration at profile end
            v1 -- Target velocity at profile end
            a_tgt -- Acceleration to use (absolute value), in transition
                     from a0, v0 to a1, v1
            jmx -- Maximum jerk value
            dt -- time step
        """

        sv = np.sign(v1-v0)
        self.dt0, self.j0, dx0, self.dta, self.ai, dxa, self.dt1, self.j1, dx1 = \
            JLAVTrans(a0, v0, a1, v1, a_tgt, jmx, dt)
        
        self.Ttotal = self.dt0 + self.dta + self.dt1

        # acceleration
        t = Symbol('t')
        _acc = Piecewise(
            (a0 + t*self.j0, t <= self.dt0),
            (self.ai, t <= self.dt0 + self.dta),
            (a1 + (t-self.Ttotal)*self.j1, True)
        )
        # evaluation function
        self._acc = lambdify(t, _acc)
        
        # integrate to velocity and position
        _vel = v0 + integrate(_acc, t)
        _x = integrate(_vel, t)

        # evaluation function
        self._vel = lambdify(t, _vel)
        self._x = lambdify(t, _x)

        if np.abs(np.abs(self.ai) - a_tgt) > _eps:
            raise ValueError("Target acceleration not achieved")

    def dt(self):
        return self.Ttotal

    def accel(self, dt):
        return self._acc(dt)

    def vel(self, dt):
        return self._vel(dt)
        
    def pos(self, dt):
        return self._x(dt)

    def json(self):
        return { 'type': 'ConstantAccelProfile', 
            'a0': float(self.accel(0.0)),
            'a1': float(self.accel(self.Ttotal)),
            'ai': self.ai,
            'v0': float(self.vel(0.0)),
            'v1': float(self.vel(self.Ttotal)),
            'Ttotal': self.Ttotal }


class ConstantSpeedProfile:

    def __init__(self, v0, duration):

        self.v0, self.Ttotal = v0, duration

    def dt(self):
        return self.Ttotal

    def accel(self, t):
        try:
            return np.zeros(t.shape)
        except AttributeError:
            return 0.0

    def vel(self, t):
        try:
            return self.v0 * np.ones(t.shape)
        except AttributeError:
            return self.v0

    def pos(self, t):
        return self.v0 * t

    def json(self):
        return { 'type': 'ConstantSpeedProfile', 
            'v0': float(self.vel(0.0)),
            'Ttotal': self.Ttotal }



class ProfileMotion:

    def __init__(self, x0: Pose, axis, profile: FadedSineProfile):
        """Create a faded-in and -out sinusoidal movement pattern.

        Arguments:
            x0 -- Initial pose of the system
            axis -- Movement axis,; axis[:3] describes linear motion distance 
                    in [m], axis[3:] describes the rotational movement size, 
                    as a rotation vector, in rad.
            profile -- FadedSineMotion profile, defines fade times, frequencies
                       and starting phases
        """
        self.x0 = x0
        self.axis = np.array(axis)
        self.profile = profile

        xyz0 = x0.xyz()
        q0 = x0.quat()

        # angular movement size
        dq = np.linalg.norm(axis[3:])
        if dq > 1e-12:
            q1 = q0 * quaternion.quaternion(
                np.cos(0.5*dq), 
                np.sin(0.5*dq)*axis[3]/dq, 
                np.sin(0.5*dq)*axis[4]/dq, 
                np.sin(0.5*dq)*axis[5]/dq)
        else:
            q1 = q0

        x1 = Pose(x0.xyz()+self.axis[:3], q1)
        self.move = x1 - x0


    def duration(self):
        """Return duration of this motion element

        Returns:
            float, duration in [s]
        """
        return self.profile.Ttotal

    def accel(self, t):
        """Return 6-element acceleration vector

        Arguments:
            t -- Time within the maneuvre

        Returns:
            np.array, 6 elements, with xdd, xdd, zdd, dwx, dwy, dwz
        """
        return self.profile.accel(t)*self.axis

    def velocity(self, t):
        return self.profile.vel(t)*self.axis

    def position(self, t):
        dx = self.profile.pos(t)
        return self.x0 + dx*self.move

    def json(self):
        return { 'type': 'ProfileMotion',
                 'from': self.x0.json(), 'move': self.move.json(),
                 'axis': self.axis.tolist(), 'profile': self.profile.json(),
                 'dt': self.duration() }

    def derivative(self, x, t):

        res = np.zeros((13, ))
        res[:6] = self.accel(t)   # deriv linear speed tensor in inertial
                                  # deriv angular vel UG wrt I, 
        res[6:9] = x[:3]          # deriv linear position in I
        W = quaternion.quaternion(0, *x[3:6]) # q for current ang vel wrt I
        q = quaternion.quaternion(*x[9:])     # for current orientation wrt I
        # dqdw = 0.5 * W * q  
        dqdw = 0.5 * q * W        # this seems the wrong one?
        # per https://www.euclideanspace.com/physics/kinematics/angularvelocity/QuaternionDifferentiation2.pdf
        # but it does work correctly.....
        res[9:] = dqdw.components # deriv current orientation
        return res


class MovementSeries:

    def __init__(self, moves):
        t = 0.0
        self.msets = OOBTree()
        for m in moves:
            print("Move of type", m.__class__.__name__,
                  "duration", m.duration(), "starting", t)
            self.msets.insert(t, m)
            t += m.duration()

        self.dt = t

    def __call__(self, t, x):
        # find the matching motion
        try:
            tm = self.msets.maxKey(t)
        except ValueError as e:
            print(f"no key looking for time {t} in {self.msets}")
            raise e

        #print(f"called for {t} with {x}, found {tm}, returning "
        #      f"{self.msets[tm].derivative(x, t - tm)}")

        # get the derivative
        return self.msets[tm].derivative(x, t - tm)

    def duration(self):
        return self.dt

    def accel(self, t):
        # find the matching motion
        try:
            tm = self.msets.maxKey(t)
        except ValueError as e:
            print(f"no key looking for time {t} in {self.msets}")
            raise e
        return self.msets[tm].accel(t - tm)

    def velocity(self, t):

        # find the matching motion
        try:
            tm = self.msets.maxKey(t)
        except ValueError as e:
            print(f"no key looking for time {t} in {self.msets}")
            raise e
        return self.msets[tm].velocity(t - tm)

    def position(self, t):

        # find the matching motion
        try:
            tm = self.msets.maxKey(t)
        except ValueError as e:
            print(f"no key looking for time {t} in {self.msets}")
            raise e
        return self.msets[tm].position(t - tm)

    def json(self):
        res = {'type': 'MovementSeries', 'moves': []}
        for t, m in self.msets.items():
            res['moves'].append({'time': t, 'move': m.json()})
        return res

if __name__ == '__main__':

    plt.close('all')

    """
    # test profile
    pr = Profile(0.8, 0.4, 2, 40)
    dt = 0.01
    t = np.arange(0, pr.dt, dt)
    a = pr.accel(t)
    v = pr.vel(t)
    x = pr.pos(t)
    f = plt.figure()
    plt.subplot(311)
    plt.plot(t, a)
    plt.subplot(312)
    plt.plot(t, v)
    plt.subplot(313)
    plt.plot(t, x)

    # test JLimMotionParts, with dt
    mp = JLimMotionParts(0.8, 0.4, 2, 40, dt=0.01)
    f = plt.figure()
    plt.subplot(211)
    plt.plot(np.cumsum([0, mp.dt0a, mp.dtaa, mp.dt1a, mp.dtc,
                        mp.dt0b, mp.dtab, mp.dt1b]),
             [0, mp.ai, mp.ai, 0, 0, -mp.ai, -mp.ai, 0])
    plt.subplot(212)
    plt.plot(np.cumsum([mp.dt0a, mp.dtaa, mp.dt1a, mp.dtc,
                        mp.dt0b, mp.dtab, mp.dt1b]),
             np.cumsum(mp.dists))

    # test with corrections, not quite clean properties, so dt's need to
    # be fixed
    mp = JLimMotionParts(0.78453, 0.41, 3, 43, dt=0.01)
    f = plt.figure()
    plt.subplot(211)
    plt.plot(np.cumsum([0, mp.dt0a, mp.dtaa, mp.dt1a, mp.dtc,
                        mp.dt0b, mp.dtab, mp.dt1b]),
             [0, mp.ai, mp.ai, 0, 0, -mp.ai, -mp.ai, 0])
    plt.subplot(212)
    plt.plot(np.cumsum([mp.dt0a, mp.dtaa, mp.dt1a, mp.dtc,
                        mp.dt0b, mp.dtab, mp.dt1b]),
             np.cumsum(mp.dists))


    # test movement, only 1 dir translation
    x0 = Pose([0,0,0])
    x1 = Pose([0.8,0,0])
    mv = Movement(x0, x1, 0.4, 2, 20, 0.5, dt=0.01)
    f = plt.figure()
    t = np.arange(0, mv.dt, 0.01)
    xyz = np.zeros((t.size,3))
    for i, tx in enumerate(t):
        xyz[i] = mv.position(tx).xyz()
    for i in range(3):
        plt.subplot(311+i)
        plt.plot(t, xyz[:,i])

    # test series
    x2 = Pose([0.8, 0, 0], [1, 0, 0])
    x3 = Pose([0.0, 0.0, 0], [0.1, 0, 0])
    ms = MovementSeries([mv,
                         Movement(x1, x2, 0.4, 2, 20, 0.5, dt=0.01),
                         Movement(x2, x3, 0.4, 2, 20, 0.5, dt=0.01),
                         Movement(x3, x0, 0.4, 2, 20, 0.5, dt=0.01)])
    t = np.arange(0, ms.duration(), 0.01)
    xyz = np.zeros((t.size,3))
    euler = np.zeros((t.size, 3))
    for i, tx in enumerate(t):
        pose_t = ms.position(tx)
        xyz[i] = pose_t.xyz()
        euler[i] = pose_t.euler()

    f = plt.figure()
    for i in range(3):
        plt.subplot(311+i)
        plt.plot(t, xyz[:,i])

    f = plt.figure()
    for i in range(3):
        plt.subplot(311+i)
        plt.plot(t, euler[:,i])
    """

    from math import cos, sin, pi
    phi = pi / 4
    a = quaternion.quaternion(cos(0.5*phi), sin(0.5*phi), 0, 0)
    b = quaternion.quaternion(cos(0.5*phi), 0, sin(0.5*phi), 0)
    pa = Pose(np.zeros((3,)), a)
    pb = Pose(np.zeros((3,)), b)
    pa2b = pb - pa

    print("a     ", pa.quat())
    print("b     ", pb.quat())
    print("a -> b", pa2b.quat())
    print("b?    ", (pa + pa2b).quat())

    # calculation as in sheet:
    z_max = 0.5
    y_max = 0.8
    x_max = 0.71

    # velocity, acceleration and jerk limits, linear
    amx = 5.3  # [m/s2], linear accel
    jmx = 47 # [m/s3], linear accel
    vmx = 0.621 # [m/s]

    rfactor = 0.5 # factor for max rotary units
    mtime = 1.0 # time still

    # collection of moves
    moves = []
    dt = 0.01

    # define positions
    ptp0 = np.zeros((3,))
    start = Pose(np.zeros((3,)), ptp0)
    x_p = Pose([x_max, 0, 0], ptp0)
    y_p = Pose([0, y_max, 0], ptp0)
    x_n = Pose([-x_max, 0, 0], ptp0)
    y_n = Pose([0, -y_max, 0], ptp0)
    moves.append(Still(start, 0.1))
    # moves.append(Movement(start, x_p, vmx, amx, jmx, rfactor, dt=dt))
    # moves.append(Still(x_p, 0.3))
    # moves.append(Movement(x_p, y_p, vmx, amx, jmx, rfactor, dt=dt))
    # moves.append(Still(y_p, 0.3))
    # moves.append(Movement(y_p, x_n, vmx, amx, jmx, rfactor, dt=dt))
    # moves.append(Still(x_n, 0.3))
    # moves.append(Movement(x_n, y_n, vmx, amx, jmx, rfactor, dt=dt))
    # moves.append(Still(y_n, 0.3))
    # moves.append(Movement(y_n, start, vmx, amx, jmx, rfactor, dt=dt))
    z_max = 0.5
    y_max = 0.8
    x_max = 0.8

    vmx0 = 0.4  # [m/s]
    amx = 4  # [m/s2], linear accel
    jmx = 1/dt # [m/s3], linear accel

    z_min = Pose([0, 0, -0.5], ptp0)
    z_max = Pose([0, 0,  0.5], ptp0)
    im = len(moves) - 1

    # move to down position
    moves.append(Movement(start, z_min, vmx0, amx, jmx, rfactor, dt))
    speeds = (0.1, 0.4, 0.7)
    accels = (amx/2, amx, amx)

    # there and back moves, different speeds
    for amx, vmx in zip(accels, speeds):
        moves.append(Movement(z_min, z_max, vmx, amx, jmx, rfactor, dt))
        moves.append(Movement(z_max, z_min, vmx, amx, jmx, rfactor, dt))

    # forward and back
    x_min = Pose([-0.45, 0, 0], ptp0)
    x_max = Pose([ 0.45, 0, 0], ptp0)

    # initial move
    moves.append(Movement(z_min, x_min, vmx0, amx, jmx, rfactor, dt))

    # now angles for cg height
    phi_max = np.radians(20)
    tht_max = np.radians(20)
    psi_max = np.radians(20)
    phi_p = Pose(np.zeros((3,)), [phi_max, 0, 0])
    tht_p = Pose(np.zeros((3,)), [0, tht_max, 0])
    phi_n = Pose(np.zeros((3,)), [-phi_max, 0, 0])
    tht_n = Pose(np.zeros((3,)), [0, -tht_max, 0])
    psi_n = Pose(np.zeros((3,)), [0, 0, -psi_max])
    psi_nx = Pose([-x_max, 0, 0], [0, 0, -psi_max])
    psi_px = Pose([x_max, 0, 0], [0, 0, psi_max])
    
    # moves.append(Movement(start, phi_p, vmx, amx, jmx, rfactor, dt=dt))
    # moves.append(Still(phi_p, mtime))
    # m2 = Movement(phi_p, tht_p, vmx, amx, jmx, rfactor, dt=dt)
    # moves.append(m2)
    # moves.append(Still(tht_p, mtime))
    # moves.append(Movement(tht_p, phi_n, vmx, amx, jmx, rfactor, dt=dt))
    # moves.append(Still(phi_n, mtime))
    # moves.append(Movement(phi_n, tht_n, vmx, amx, jmx, rfactor, dt=dt))
    # moves.append(Still(tht_n, mtime))
    # moves.append(Movement(tht_n, psi_nx, vmx, amx, jmx, rfactor, dt=dt))
    # moves.append(Still(psi_nx, mtime))
    # moves.append(Movement(psi_nx, psi_px, vmx, amx, jmx, rfactor, dt=dt))
    # moves.append(Still(psi_px, mtime))
    # moves.append(Movement(psi_px, start, vmx, amx, jmx, rfactor, dt=dt))   

    #moves.append(Movement(tht_p, phi_n, vmx, amx, jmx, rfactor, dt=dt))
    ##moves.append(Still(phi_n, mtime))
    #moves.append(Movement(phi_n, start, vmx, amx, jmx, rfactor, dt=dt))

    # sine motion profile, linear
    # fader0 = FadedSineProfile(3.0, 6.0+np.pi, [2, 0.6], [1.0, 1.0], [0.3*np.pi/2, 0.0])

    # moves.append(ProfileMotion(start, [0.2, 0.1, 0.0, 0.0, 0.0, 0.0], fader0))
    # moves.append(Movement(start, phi_p, vmx, amx, jmx, rfactor, dt=dt))
    # moves.append(ProfileMotion(phi_p, [0, 0, 0, 0.0, 0.05, 0.1], fader0))
    # moves.append(Movement(phi_p, start, vmx, amx, jmx, rfactor, dt=dt))

    accmove0 = ConstantAccelProfile(0.0, 0.0, 0.0, 0.1, 1.0, 50, 0.01)
    dt = 0.001
    time = np.arange(0, accmove0.Ttotal, dt)
    

    accmove1 = ConstantAccelProfile(0.0, 0.1, 0.0, -0.1, 0.1, 50, 0.01)

    moves.append(ProfileMotion(start, [1,0,0,0,0,0], accmove0))
    moves.append(ProfileMotion(moves[-1].position(moves[-1].duration()),
        [1,0,0,0,0,0], accmove1))
    print(moves[-1].position(moves[-1].duration()))

    # create a derivative function
    fn = MovementSeries(moves)

    # json?
    print(fn.json())

    # calculate the driving data file;
    dt = 0.001
    time = np.arange(0, fn.duration(), dt)
    sign = np.zeros((time.shape[0], 18))

    # signal; x, y, z, phi, tht, psi, u, v, w, p, q, r, ud, vd, wd, pd, qd, rd
    for i, t in enumerate(time):
        pose = fn.position(t)
        sign[i,:3] = pose.xyz()
        sign[i,3:6] = pose.euler()
        sign[i,6:12] = fn.velocity(t)
        sign[i,12:] = fn.accel(t)

    # f = plt.figure()
    # for i, n in enumerate('xyz'):
    #     plt.subplot(3, 1, i+1)
    #     plt.plot(time, sign[:,i], label=n)
    #     plt.legend()

    # f = plt.figure()
    # for i, n in enumerate((r'$\phi$', r'$\theta$', r'$\psi$')):
    #     plt.subplot(3, 1, i+1)
    #     plt.plot(time, sign[:,3+i], label=n)
    #     plt.legend()

    # with integration.
    # the acceleration (derivative of 1st 6 states, UVW, PQR), is
    # calculated from the movement; same as above.
    # velocity still needs to be verified
    from scipy.integrate import RK45
    X = np.zeros((time.size, 13))
    X[0] = start.x
    erangles = np.zeros((time.size, 3))

    # integrate
    for i in range(len(time)-1):
        res = RK45(fn, time[i], X[i], time[i+1])
        res.step()
        res.y[9:] = res.y[9:]/np.linalg.norm(res.y[9:])
        X[i+1] = res.y
        erangles[i+1] = rodriguez(quaternion.quaternion(*res.y[9:]))

    f = plt.figure()
    for i, n in enumerate('xyz'):
        plt.subplot(3, 1, i+1)
        plt.plot(time, X[:,6+i], label=n)
        plt.plot(time, sign[:,i], label=f"r_{n}")
        plt.legend()

    f = plt.figure()
    for i, n in enumerate((r'$\phi$', r'$\theta$', r'$\psi$')):
        plt.subplot(3, 1, i+1)
        plt.plot(time, erangles[:,i], label=n)
        plt.plot(time, sign[:,3+i], label=f"r_{n}")
        plt.legend()

    f = plt.figure()
    for i, n in enumerate('UVW'):
        plt.subplot(3, 1, i+1)
        plt.plot(time, X[:,i], label=n)
        plt.plot(time, sign[:,6+i], label=f"r_{n}")
        plt.legend()

    f = plt.figure()
    for i, n in enumerate('PQR'):
        plt.subplot(3, 1, i+1)
        plt.plot(time, X[:,3+i], label=n)
        plt.plot(time, sign[:,9+i], label=f"r_{n}")
        plt.legend()

    plt.show()

