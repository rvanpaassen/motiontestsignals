#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 18:28:11 2022

@author: repa
"""

import json
import bz2
import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import lstsq

def dprint(*args, **kwargs):
    #print(*args, **kwargs)
    pass

dof_index = {'X': 0, 'Y': 1, 'Z': 2, 'phi': 3, 'tht': 4, 'psi': 5}

def match2Ideal(y, dts, dof, accl, ax0=None, ax1=None):
    """
    Comparison of parabolic signal y to ideal parabola. A linear 
    least-squares fit is made

    Parameters
    ----------
    y : array of float
        Measured data, to be matched.
    dts : float
        Time step.
    dof : string
        Name/label of the degree of freedom tested.
    accl : float
        Nominal acceleration level.
    ax0 : Matplotlib axis handle, optional
        If given, will be used to plot the nominal estimate and the
        measured data. The default is None.
    ax1 : Matplotlib axis handle, optional
        If given, will be used to plot the error between estimate and
        measured data. The default is None.

    Returns
    -------
    float
        Acceleration level, estimated.
    float
        Acceleration peak time, relative to start of data.
    res : float
        Residue.

    """
    
    # the given signal should match an acceleration/parabola
    npt = len(y)
    tvec = np.arange(0, dts*npt, dts)
    
    # prepare for linear regression
    X = np.ones((npt,3))
    X[:,1] = tvec
    X[:,2] = X[:,1]**2
            
    b, res, rank, singlr = lstsq(X, y.reshape((-1,1)), rcond=None)

    if ax0 is not None:
        # plot the two signals
        ax0.plot(tvec, (X @ b).reshape((-1,)),
                 label="parabolic/constant acc")
        ax0.plot(tvec, y,
                 label=f"output {dof} at {accl:.3f}")
    if ax1 is not None:
        # plot the resulting error
        ax1.plot(tvec, (X @ b).reshape((-1,)) - y,
                 label="parabolic/constant acc")

    # acceleration value in b[2], peak of the movement at -b[1]/(2b[0])
    # residue of the position fit returned
    return 2*float(b[2]), -float(b[1])/(2*float(b[2])), float(res)


def bumpAnalysis(d, dts, dt, dofs, moves, accels, speeds):
    
    results = []
    
    for dof in dofs:
        

        idof = dof_index[dof]

        # select the data    
        u = d[:,38+idof]
        y = d[:,74+idof]
        t = np.arange(0, dts * len(u), dts)

        # first find the time correction for minimum difference
        fig = plt.figure()
        ax = fig.add_subplot(title=f"Raw data for motion bump analysis {dof}")
        ax.plot(t, u, label="driven input")
        ax.plot(t, y, label="result")

        # find delay to minimize diff
        iodiff = np.zeros((10,))
        idel = np.array(range(1,11))
        for idl in idel:
            iodiff[idl-10] = np.sum(np.abs(u[:-idl]-y[idl:]))
            

        fig = plt.figure()
        ax = fig.add_subplot(
            title=f"absolute difference against delay steps {dof}")
        ax.plot(idel, iodiff)
        idxmin = np.where(iodiff == np.min(iodiff))[0][0]
        
        # find the index of the first non-zero data, and skip the initial
        # positioning move
        nstep = int(round(dts/dt))
        idxco = np.where(u != 0)[0][0] -1
        print(f"Input in dof {dof} starts at index {idxco+1}")
        idxco = idxco*nstep
        
        # first move is skipped, account for the points
        idx = idxco + sum(moves[0][0])

        r1 = [ dof, idxmin ]
        
        # analyse different v/a combinations, first make nice plots
        mv = iter(moves[1:-4])
        for speed, acc in zip(speeds, accels):

            # get the data on the initial movement
            (n0, na, n1), accl = next(mv)
            
            # and on the reverse
            (n0r, nar, n1r), acclr = next(mv)
            # this may be a coast, coast over in that case
            if acclr == 0.0:
                dprint(f"coasting for {nar*dt} seconds")
                ncoast = n0r+nar+n1r
                (n0r, nar, n1r), acclr = next(mv)
            else:
                ncoast = 0
                #idx += n0 + na + n1
            
            # check that we have data
            if (idx + n0 + na + n1 + ncoast + n0r + nar + n1r) // nstep > \
                len(y):
                print(f"Data file ended before completing level {accl}")
                break
                
            
            # skip over jerk phase
            idx += n0
            
            # when the negative acceleration is larger than the return one, 
            # pick only the last part of data
            if na > nar:
                dprint(f"at acc {accl}/{acclr}, reducing {na} to {nar}")
                idx += na - nar
                na = nar
            else:
                dprint(f"at acc {accl}/{acclr}, have {na} and {nar}")
                
            # fig = plt.figure()       # measured data against nominal, delayed
            figm = plt.figure()      # measured data against ideal fit
            
            # data rate indices
            i2 = idx // 20 + 1
            di = na // 20
            
            # move to past the signal
            idx += na + n1
            
            # create a quadratic fit over the given range
            axm0 = figm.add_subplot(3, 1, 1,
                title=f"ideal fit and measured {dof}, acc={accl:.3f}")
            axm1 = figm.add_subplot(3, 1, 2,
                title=f"ideal - measured, {dof}, acc={accl:.3f}")
            accest, t_est, res = match2Ideal(
                y[i2+idxmin:i2+idxmin+di], dts, dof, accl, axm0, axm1)
            # print(f"Accel {accl:.4f}, est acc {accest:.4f}, residue {res}")
            # print(f"Peak time at {t_est}")
            r1.append([accl, accest, t_est, res])
            
            # skip the coast phase
            dprint(f"skipping coast points {ncoast}")
            idx += ncoast                
            
            # to the reverse test signal
            idx += n0r
            i2 = idx // 20 + 1
            di = nar // 20
            dprint(f"reverse, {di} points at {acclr}")
            
            # print(f"acceleration level {accl}")
            # ax0.plot(np.arange(0, dts*di, dts), u[i2:i2+di],
            #         label=f"input u at {accl:.3f}")
            # ax0.plot(np.arange(0, dts*di, dts), y[i2+idxmin:i2+idxmin+di],
            #         label="output y at {accl:.3f}")

            # ax = fig.add_subplot(3, 1, 3,
            #    title=f"error signal {dof}, acc={accl:.3f}, v0={speed:.3f}")
            # ax.plot(np.arange(0, dts*di, dts),
            #     u[i2:i2+di] - y[i2+idxmin:i2+idxmin+di])     
            # fig.tight_layout() 

            axm2 = figm.add_subplot(3, 1, 3,
                title=f"ideal - measured, {dof}, acc={acclr:.3f}")
            accest, t_est, res = match2Ideal(
                y[i2+idxmin:i2+idxmin+di], dts, dof, acclr, axm0, axm2)
            # print(f"Accel {accl:.4f}, est acc {accest:.4f}, residue {res}")
            # print(f"Peak time at {t_est}")
            r1.append([acclr, accest, t_est, res])
            figm.tight_layout()

            # past this signal
            idx += nar + n1r
            
        # collect results
        results.append(r1)
            
    return results    


if __name__ == '__main__':

    # test and debug, create symbolic links to data files

    plt.close('all')    

    # open the data file
    fname = "motion-run20220506.log.bz2"
    with bz2.open(fname, 'r') as f:
        data = np.loadtxt(f)

    # correction for z:
    data[:,74+2] = -data[:,74+2] + 2.39

    # read the moves definition; gives movement times based on a 2000 Hz 
    # setpoint generation; this is later re-sampled to 100 Hz for SRS
    with open('bumpmoves.json', 'r') as fm:
        mspec = json.load(fm)
        moves = mspec['moves']
        speeds = mspec['speeds']
        accels = mspec['accelerations']
        dofs = mspec['dofs']
        dts = mspec['dts']
        dt = mspec['dt']
    
    t_pmd = 1180.0
    offset = int(round(t_pmd/dts))
    
    # fig = plt.figure()
    # ax = fig.add_subplot()
    # ax.plot(data[offset:,38+2])
    # ax.plot(data[offset:,74+2])
    
    result = bumpAnalysis(data[offset:,:], dts, dt, dofs, moves, accels, speeds)   
    
