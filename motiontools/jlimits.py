import numpy as np

_eps = 1e-13

def JLATrans(a0: float, a1: float, jmx: float, dt: float):
    """Rounded, jerk-limited transition between two acceleration levels

    Arguments:
        a0 -- Initial acceleration
        a1 -- Final acceleration
        jmx -- Maximum jerk
        dt -- Time step size

    Returns:
        -- Duration of transition, rounded up
        -- Adjusted maximum jerk value (abs should be <= jmx)
        -- Velocity change over the maneuvre
        (dtj, jmx2, dv)
    """
    da = np.abs(a1 - a0)
    sda = np.sign(a1 - a0)
    nsteps = np.ceil(da/jmx/dt-_eps)

    return nsteps*dt, (a1-a0)/(nsteps*dt), 0.5*nsteps*dt*(a1+a0)


def JLAVTrans(a0: float, v0: float, a1: float, v1: float, 
              amx: float, jmx: float, dt: float):
    """Rounded, jerk and acceleration limited transition from one
       acceleration/velocity combination to a second acceleration/velocity
       combination

    Arguments:
        a0 -- _description_
        v0 -- _description_
        a1 -- _description_
        v1 -- _description_
        amx -- Maximum intermediate acceleration level
        jmx -- _description_
        dt -- _description_

    Returns:
        tuple with
        - constant jerk transition time
        - jerk
        - constant acceleration time
        - intermediate acceleration
        - constant jerk transition time
        - jerk
    """
    # integration of the two jerk transitions and the intermediate acceleration
    # must be equal to the delta-v over the transition
    
    # jerk or acceleration limited?
    dv = np.abs(v1 - v0)
    sv = np.sign(v1 - v0)

    # times constant jerk transitions
    tj0, j0, dv0 = JLATrans(a0, sv*amx, jmx, dt)
    tj1, j1, dv1 = JLATrans(sv*amx, a1, jmx, dt)

    # initial guess coasting time
    tai = (v1 - v0 - dv0 - dv1) / (sv*amx)

    if tai > 0.0:

        # round it off
        ta = np.ceil(tai/dt-_eps)*dt

        # re-scale the jerk and intermediate acceleration level to correct for
        # the extra (ta-tai)*amx velocity change
        #fct = (dv - (tj0+tj1)*0.5*np.abs(a0+a1)) / \
        #    (dv - (tj0+tj1)*0.5*np.abs(a0+a1) + (ta-tai)*amx)
        da = (ta-tai)*amx/(ta+0.5*(tj0+tj1))
        #fct = (amx - da) / amx
        j0, j1, ai = j0 - sv*da/tj0, j1 + sv*da/tj1, sv*amx - sv*da

    else:

        # no intermediate accel
        ta = 0.0

        # re-scale the jerk phases to correct for the excess velocity change
        fct = (v1-v0)/(dv1 + dv0)
        j0, j1 = fct*j0, fct*j1
        ai = a0 + tj0*j0

    # check?
    vn = v0 + tj0 * (a0+0.5*tj0*j0) + ta * ai + tj1 * (ai+0.5*tj1*j1)
    an = ai + tj1*j1
    if np.abs(v1 - vn) > 10*_eps or np.abs(a1-an) > 10*_eps:
        raise ValueError(f"Calculation error v1={v1}, vn={vn}, a1={a1}, an={an}")

    va = v0+(a0+0.5*j0*tj0)*tj0
    dx0 = (v0 + 0.5*a0*tj0 + 1.0/6.0*j0*tj0**2)*tj0
    dxa = (va + 0.5*ai*ta)*ta
    dx1 = (v1 - 0.5*a1*tj1 - 1.0/6.0*j1*tj1**2)*tj1

    return (tj0, j0, dx0,
            ta, ai, dxa,
            tj1, j1, dx1)

if __name__ == '__main__':

    print(JLATrans(0.0, 1.0, 30.0, 0.01))
    print(JLATrans(1.0, 0.0, 40.0, 0.01))
    print(JLATrans(1.0, -1.0, 50.0, 0.01))
    print(JLATrans(1.0, 2.0, 60.0, 0.01))
    print(JLATrans(-0.5, 1.5, 23.0, 0.01))

    print(JLAVTrans(0.0, 0.0, 0.0, 0.8, 5.0, 70.0, 0.01))
    print(JLAVTrans(0.0, 0.8, 0.0, 0.0, 5.0, 70.0, 0.01))
    # nonzero initial acceleration, final acceleration
    print(JLAVTrans(1.0, 0.0, 0.5, 0.5, 5, 60, 0.01))

    # short, not reaching amx
    print(JLAVTrans(0.0, 0.0, 0.0, 0.1, 5, 50, 0.01))
