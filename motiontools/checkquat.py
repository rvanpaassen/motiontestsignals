#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 30 10:39:01 2022

@author: repa
"""

from quaternion import quaternion, from_euler_angles
from math import sin, cos, pi, asin
import numpy as np

phi = pi/4
a = quaternion(cos(0.5*phi), sin(0.5*phi), 0, 0)
b = quaternion(cos(0.5*phi), 0, sin(0.5*phi), 0)

def euler(q):
        # remember: w:9, x:10, y:11, z:12
        return np.array((
            np.arctan2(2.0*(q.y*q.z+q.x*q.w),
                       q.w*q.w-q.x*q.x-q.y*q.y+q.z*q.z),
            np.arcsin(-2.0*(q.x*q.z-q.y*q.w)),
            np.arctan2(2.0*(q.x*q.y+q.w*q.z),
                       q.w*q.w+q.x*q.x-q.y*q.y-q.z*q.z)))



print(from_euler_angles(phi, 0, 0))
print(from_euler_angles(0, phi, 0))
print(from_euler_angles(0, 0, phi))
print()
print("b     ", b)
print(b.conjugate())
print(b*b.conjugate())
print(a*b)

a_to_b = b * a.conjugate()
print("a -> b", a_to_b)
print("b?    ", a_to_b*a)

print()
for f in np.arange(0,1.01,0.1):
    print("fraction", np.round(f), b**f,
          np.round(2.0*np.degrees(asin((b**f).y)),decimals=2))
    
for f in np.arange(0, 1.01, 0.1):
    qn = (a_to_b**f)*a
    print("trans", np.round(f,decimals=1), qn, 
          np.round(np.degrees(euler(qn)),decimals=1))