#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 10:57:08 2022

@author: repa
"""
import h5py
import matplotlib.pyplot as plt

plt.close('all')

for fl in ("run_2_trim.mat", "run_3_trim.mat"):
    data = h5py.File(f'../notebooks/{fl}', 'r')
    print(data.keys())
    tm = data['time'][0]
    
    # actuator length plots
    dL = data['dL']
    dP = data['dP']
    setPoint = data['setPoint']
    
    f = plt.figure()
    f.suptitle(f'Actuator lengths {fl}')
    for i in range(6):
        plt.subplot(6, 1, i+1)
        plt.plot(tm, dL[i])
    
    # pressure plots
    f = plt.figure()
    f.suptitle(f'dP pressure {fl}')
    for i in range(6):
        plt.subplot(6, 1, i+1)
        plt.plot(tm, dP[i])
        
    # setpoint plots
    f = plt.figure()
    f.suptitle(f'Setpoints {fl}')
    for i in range(6):
        plt.subplot(6, 1, i+1)
        plt.plot(tm, setPoint[i])

    plt.show()
    
    