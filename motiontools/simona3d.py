import open3d as o3d
from Platform import Platform, quat_from_er
import numpy as np

class SRSVisual(Platform):

    def __init__(self, l_inner, l_outer, scene, *args, **kwargs):
        super(SRSVisual, self).__init__(*args, **kwargs)

        self.l_inner, self.l_outer = l_inner, l_outer

        
        # Lower gimbals visualized as 6 dodecahedrons for now
        self.lower_gimbals = [
            o3d.geometry.TriangleMesh.create_icosahedron(radius=0.15)
            for i in range(6)
        ]
        # upper gimbals too
        self.upper_gimbals = [
            o3d.geometry.TriangleMesh.create_icosahedron(radius=0.075)
            for i in range(6)
        ]

        # inner and outer cylinders
        self.cylinders = [
            o3d.geometry.TriangleMesh.create_cylinder(
                radius=0.2, height=l_outer) for i in range(6)
        ]
        self.pistons = [
            o3d.geometry.TriangleMesh.create_cylinder(
                radius=0.05, height=l_inner) for i in range(6)
        ]

        # normals for all
        self.allgeoms = []
        for g in (self.lower_gimbals, self.upper_gimbals, 
            self.cylinders, self.pistons):
            for o in g:
                o.compute_vertex_normals()
                self.allgeoms.append(o)

        # locate all moving parts
        self.setPose(np.zeros((3,)), quat_from_er(0, 0, 0))
        self.updateVis()

        # Material
        #lgmat = o3d.visualization.rendering.MaterialRecord()
        self.gmlist = []
        lgmat = o3d.visualization.rendering.MaterialRecord()
        ugmat = o3d.visualization.rendering.MaterialRecord()
        cylmat = o3d.visualization.rendering.MaterialRecord()
        pismat = o3d.visualization.rendering.MaterialRecord()
        lgmat.base_color = np.array([0.5, 0.7, 0.4, 1.0])
        lgmat.shader='defaultLit'
        ugmat.base_color = (0.8, 0.8, 0.8, 1.0)
        ugmat.shader='defaultLit'
        cylmat.base_color = (1, 0.7, 0, 0.6)
        cylmat.shader='defaultLit'
        pismat.base_color = (0.3, 0.3, 0.5, 1.0)
        pismat.shader='defaultLit'

        # Initial colors, and locations for fixed parts
        for i in range(6):
            
            # fixed location lower gimbals
            self.lower_gimbals[i].translate(self.LGgeom[:,i])

            self.gmlist.append((f"lg{i}", self.lower_gimbals[i], lgmat))
            self.gmlist.append((f"ug{i}", self.upper_gimbals[i], ugmat))
            self.gmlist.append((f"cyl{i}", self.cylinders[i], cylmat))
            self.gmlist.append((f"pis{i}", self.pistons[i], pismat))

        self.rewriteGeometry(scene.scene)

    def rewriteGeometry(self, scene):
        scene.clear_geometry()
        for n, g, m in self.gmlist:
            scene.add_geometry(n, g, m)

    def updateVis(self):
        for i in range(6):
            self.upper_gimbals[i].translate(self.UGpos[:,i], relative=False)

            c = self.Junit[2,i]
            V = np.array([[0,0, self.Junit[0,i]],[0,0,self.Junit[1,i]],
                [-self.Junit[0,i], -self.Junit[1,i], 0]])
            R = np.eye(3) + V + V@V*1/(1+c)
            #self.cylinders[i].rotate(R)
            #self.pistons[i].rotate(R)

            self.cylinders[i].translate(self.LGgeom[:,i] + self.Junit[:,i]*(0.5*self.l_outer+0.2), relative=False)
            self.pistons[i].translate(self.UGpos[:,i] - self.Junit[:,i]*(0.5*self.l_inner+0.2), relative=False)

        pass


class TestUpdate:

    def __init__(self, v):

        self.phase = 0
        self.t = 0.0
        self.x = np.zeros((6,))
        self.v = v

    def update(self):
        if self.t <= 2.0:
            self.x[self.phase] = 0.5 * np.sin(self.t*np.pi)
            self.t = self.t + 0.02
            print(f"t={self.t}")
        else:
            self.phase = (self.phase + 1) % 6
            self.x = np.zeros((6,))
            self.t = 0.0
            print(f"Phase {self.phase}")

        self.v.setPose(self.x[:3], quat_from_er(self.x[3:]))
        self.v.updateVis()

if __name__ == '__main__':
    app = o3d.visualization.gui.Application.instance
    o3d.visualization.gui.Application.instance.initialize()

    win = o3d.visualization.gui.Application.instance.create_window(
        'SRS 3d view', 1024, 768)
    
    scene = o3d.visualization.gui.SceneWidget()
    scene.scene = o3d.visualization.rendering.Open3DScene(win.renderer)
    scene.scene.scene.set_sun_light(
            [-1, -1, 1],  # direction
            [1, 1, 1],  # color
            100000)
    scene.scene.scene.enable_sun_light(True)
    bbox = o3d.geometry.AxisAlignedBoundingBox([-3, -3, -3],
                                                [3, 3, 3])
    scene.setup_camera(50.0, bbox, np.array((0,0,0)))
    scene.look_at([0, 0, 1], [-5, 3.5, -2], [0, 0, -1] )
    win.add_child(scene)
    #win.ground_plane=o3d.visualization.rendering.Scene.GroundPlane.XY

    v = SRSVisual(1.25, 1.4, scene, 2.131, 3.281, 1.6, 1.65, 
        2*np.arcsin(1.0/16.0), 2*np.arcsin(3.0/16.5), 2.39)
    tu = TestUpdate(v)

    def do_rewrite():
        v.rewriteGeometry(scene.scene)

    while app.run_one_tick():
        tu.update()
        print(v.UGpos)
        # v.rewriteGeometry(scene.scene)
        app.post_to_main_thread(win, do_rewrite)
        win.post_redraw()
    print("app run done")
