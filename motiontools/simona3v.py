import vpython as vp
from Platform import Platform, quat_from_er
import numpy as np
from quaternion import as_rotation_matrix

class SRSVisual(Platform):

    def __init__(self, l_inner, l_outer, *args, **kwargs):
        super(SRSVisual, self).__init__(*args, **kwargs)

        self.l_inner, self.l_outer = l_inner, l_outer

        
        # Lower gimbals visualized as 6 dodecahedrons for now
        self.lower_gimbals = [
            vp.sphere(radius=0.15, color=vp.vector(0.5, 0.7, 0.4))
            for i in range(6)
        ]
        # upper gimbals too
        self.upper_gimbals = [
            vp.sphere(radius=0.075, color=vp.vector(0.8, 0.8, 0.8))
            for i in range(6)
        ]

        # inner and outer cylinders
        self.cylinders = [
            vp.cylinder(radius=0.2, height=l_outer, color=vp.vector(1, 0.7, 0),
                opacity=0.6)
            for i in range(6)
        ]
        self.pistons = [
            vp.cylinder(radius=0.05, height=l_inner, color=vp.vector(0.3, 0.3, 0.5)) 
            for i in range(6)
        ]

        # locate all moving parts
        self.setPose(np.zeros((3,)), quat_from_er(0, 0, 0))
 
        # Initial colors, and locations for fixed parts
        for i in range(6):
            
            # fixed location lower gimbals
            self.lower_gimbals[i].pos = vp.vector(*self.LGgeom[:,i])

        # seats
        seatbase = vp.box(size=vp.vector(0.35,0.4,0.07), pos=vp.vector(0.2,0,0),
                color=vp.vector(0.4, 0.4, 0.7))
        seatback = vp.box(size=vp.vector(0.05,0.4,0.6), pos=vp.vector(-0.1,0.,-0.3),
                color=vp.vector(0.4, 0.4, 0.7), axis=vp.vector(0.8,0.0,-0.2))
        leftseat = vp.compound([seatbase, seatback], pos=vp.vector(0.0, -0.6, 0))
        rightseat = vp.compound([seatbase, seatback], pos=vp.vector(0.0, 0.6, 0))
        self.seats = vp.compound([leftseat, rightseat], pos=vp.vector(0.0, 0.0, -2))

        self.updateVis()

    def updateVis(self):
        for i in range(6):
            self.upper_gimbals[i].pos = vp.vector(*self.UGpos[:,i])

            self.cylinders[i].pos = vp.vector(
                *(self.LGgeom[:,i] + self.Junit[:,i]*(0.2)))
            self.cylinders[i].axis = vp.vector(
                *(self.Junit[:,i]*self.l_outer))
            self.pistons[i].pos = vp.vector(
                *(self.UGpos[:,i] - self.Junit[:,i]*(0.2)))
            self.pistons[i].axis = vp.vector(
                *(-self.Junit[:,i]*self.l_inner))
        dseats = as_rotation_matrix(self.qcmd) @ np.array((0, 0.0, -0.5))
        self.seats.axis = vp.vector(1,0,0)
        self.seats.up = vp.vector(0,1,0)
        self.seats.pos = vp.vector(*(self.xcmd+dseats))
        self.seats.rotate(angle = 2*np.arccos(self.qcmd.w), axis=vp.vector(*self.qcmd.vec),
            origin = vp.vector(*self.xcmd))
        #print(self.seats.axis)


class TestUpdate:

    def __init__(self, v):

        self.phase = 0
        self.t = 0.0
        self.x = np.zeros((6,))
        self.v = v

    def update(self):
        if self.t <= 2.0:
            self.x[self.phase] = 0.5 * np.sin(self.t*np.pi)
            self.t = self.t + 0.02
            #print(f"t={self.t}")
        else:
            self.phase = (self.phase + 1) % 6
            self.x = np.zeros((6,))
            self.t = 0.0
            print(f"Phase {self.phase}")

        self.v.setPose(self.x[:3], quat_from_er(self.x[3:]))
        self.v.updateVis()

def setupScene():
    # vp.scene.caption = 'SRS 3d view'
    vp.scene.forward = vp.vector(3,-2,0)
    vp.scene.ambient = vp.color.gray(0.5)
    vp.scene.lights = [ ]
    sun = vp.distant_light(direction = vp.vector(1, -0.5, -2), 
            color=vp.color.gray(0.95))
    vp.scene.camera.pos = vp.vector(-3,1,-0.5)
    vp.scene.camera.up = vp.vector(0, 0, -1)


if __name__ == '__main__':

    setupScene()
    v = SRSVisual(1.25, 1.4, 2.131, 3.281, 1.6, 1.65, 
        2*np.arcsin(1.0/16.0), 2*np.arcsin(3.0/16.5), 2.39)
    tu = TestUpdate(v)

    data = np.loadtxt('../notebooks/srs-test-motion-cg.dat')
    while True:
        for d in data:
            vp.rate(60)
            v.setPose(d[:3], quat_from_er(d[3:6]))
            v.updateVis()
        #tu.update()
        #print(v.UGpos)
        # v.rewriteGeometry(scene.scene)
    print("app run done")
