#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 10:57:08 2022

@author: repa
"""
import h5py
import matplotlib.pyplot as plt
from Platform import Platform

plt.close('all')

for fl in ("run_2_trim.mat",):
    data = h5py.File(f'../notebooks/{fl}', 'r')
    print(data.keys())
    tm = data['time'][0]
    
    # actuator length plots
    dL = data['dL']
    dP = data['dP']
    setPoint = data['setPoint']
    
    f = plt.figure()
    f.suptitle(f'Actuator lengths {fl}')
    for i in range(6):
        plt.subplot(6, 1, i+1)
        plt.plot(tm, dL[i])
    
    # pressure plots
    f = plt.figure()
    f.suptitle(f'dP pressure {fl}')
    for i in range(6):
        plt.subplot(6, 1, i+1)
        plt.plot(tm, dP[i])
        
    # setpoint plots
    f = plt.figure()
    f.suptitle(f'Setpoints {fl}')
    for i in range(6):
        plt.subplot(6, 1, i+1)
        plt.plot(tm, setPoint[i])

plt.show()


    
# base time 20.48 s, with a 20.48/3 period (6.76 s, 1.075 Hz)
# total time each 40 s, clip off 10 s at start, to remove fade-in
import math as m
import numpy as np

wtest = 1/(20.48/3)*2*m.pi

# srs platform
srs = Platform(2.131, 3.281, 1.6, 1.65, 
               2*np.arcsin(1.0/16.0), 2*np.arcsin(3.0/16.5), 2.39)

# time base runin
t_offset = 8.7
t_period = 40.0
t_measure = 20.48
t_clip = 10

# get initial length, because measurements are dL
q = np.zeros((7,)); q[3] = 1.0
L0 = np.mean(srs.getL(q))

# pressure in "bar"/100KPa
factf = 100e3 * 2.5e-3

# create a phi, theta, psi, x, y, z reconstruction
npts = dL.shape[1]
r_q = np.zeros((npts, 7))

print(f"number of points to analyze: {dL.shape}")
for i in range(npts):
    q = srs.getPoseFromLength(dL[:,i] + L0, q)
    r_q[i] = q
    print(i, q)

for ix, ax in enumerate('xyz'):
    t_base = t_offset + t_clip + ix * t_period
    t_select = (tm > t_base) * (tm <= t_base + t_measure)
    t = np.array(tm)[t_select]
    dP = np.array(data['dP'])[:,t_select]
    dL = np.array(data['dL'])[:,t_select]
    q = np.zeros((7,)); q[3] = 1.0
    M = np.zeros(t.shape)
    for i in range(t.size):
        q = srs.getPoseFromLength(dL[:,i]+L0, q)
        fm = srs.cartForcesMoments(q, dP[:,i]*factf)
        M[i] = fm[3+ix]
    
    f = plt.figure()
    plt.plot(t, M)

plt.show()
        

