#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 15:24:51 2022

@author: repa
"""

import numpy as np
import quaternion
from motiongen import JlimMotionProperty, JlimMotionSolution
from BTrees.OOBTree import OOBTree
from matplotlib import pyplot as plt
from sympy import Symbol





_eps = 1e-11

def dprint(*args, **kwargs):
    print(*args, **kwargs)
    pass

class Pose:
    """Define an attitude/position and its derivatives for a moving body

    Derivatives (velocity and rotational speed), are defined in a fixed
    base coordinate system, not in the body coordinate system!
    """
    def __init__(self, *args):
        """Set an initial attitude and position

        Arguments:
        - Option 1, a 13-element iterable, containing all states, in the
          following order; 
          * uvw, linear speeds in fixed axis system
          * omg, rotation vector in fixed axis system
          * xyz, position
          * w, i, j, k, orientation quaternion
        - Option 2, a 3 element iterable and a quaternion object, defines
          xyz and the orientation quaternion
        - Option 3, a 3 element iterable and 4 elements, as above, interprets
          the 4 elements as the quaternion
        - Option 4, 4 elements, interprets these as the quaternion
        - Option 5, a 3 element iterable and a 3 element iterable, interprets
          the orientation as phi, theta, psi
        """
        
        # states:
        # uvw / linear speeds
        # pqr / omega
        # xyz
        # quat
        if len(args) == 1 and len(args[0]) == 13:
            self.x = np.array(args[0])
        if len(args) >= 1 and len(args[0]) == 3:
            self.x = np.zeros((13,))
            self.x[6:9] = np.array(args[0])
            if len(args) >= 2:
                if isinstance(args[1], quaternion.quaternion):
                    self.x[9:] = args[1].components
                elif len(args[1]) == 3:
                    # phi, theta, psi
                    cf, sf = np.cos(0.5*args[1][0]), np.sin(0.5*args[1][0])
                    ct, st = np.cos(0.5*args[1][1]), np.sin(0.5*args[1][1])
                    cp, sp = np.cos(0.5*args[1][2]), np.sin(0.5*args[1][2])
                    self.x[9] = cf*ct*cp + sf*st*sp
                    self.x[10] = sf*ct*cp - cf*st*sp
                    self.x[11] = cf*st*cp + sf*ct*sp
                    self.x[12] = cf*ct*sp - sf*st*cp
                    # self.x[9:] = quaternion.from_euler_angles(args[1]).components
                elif len(args[1]) == 4:
                    self.x[9:] = np.quaternion(*args[1]).components
                else:
                    self.x[9:] = np.quaternion(args[1]).components
    
    def quat(self):
        """Obtain attitude

        Returns:
            Attitude quaternion
        """
        return quaternion.quaternion(*self.x[9:])
    
    def xyz(self):
        """Obtain position

        Returns:
            3-element vector with position
        """
        return self.x[6:9]
    
    def __add__(self, b):
        """Add two poses

        Arguments:
            b -- sums the two positions, and applies 

        Returns:
            _description_
        """
        return Pose(self.xyz() + b.xyz(), self.quat() * b.quat())
    
    def __sub__(self, b):
        return Pose(self.xyz() - b.xyz(), self.quat() * b.quat().inverse())

    def __repr__(self):
        return f"Pose({self.xyz()}, {self.quat()})"

    def __str__(self):
        return f"Pose({self.xyz()}, {self.quat()})"
    
    def __mul__(self, factor):
        return Pose(factor*self.xyz(), self.quat() ** factor)
        
    def __rmul__(self, factor):
        return self.__mul__(factor)
    
    def angle(self):
        return 2*np.arccos(self.x[9])
    
    def axis(self):
        n = np.linalg.norm(self.x[10:])
        if n > 1e-15:
            return self.x[10:] / n
        return np.zeros((3,))
            
    def derivative(self, u):
        res = np.zeros((13, ))
        res[:6] = u
        res[6:9] = self.x[:3]
        res[9] = -0.5 * np.inner(self.x[3:6], self.x[10:])
        res[10:] = 0.5 * self.x[9]*self.x[3:6] - \
            np.cross(self.x[3:6], self.x[10:])
        return res
    
        
class Motion:
    
    def __init__(self, acc0, dacc):
        self.acc0 = acc0
        self.dacc = dacc
        
    def accel(self, t):
        return self.acc0 + self.dacc*t
    
class JLimMotionParts:
    
    def __init__(self, dist, vmx=0.1, amx=1.0, jmx=1.0, leader=None,
                 tcoast=0.0, dt=0.0):
        
        if leader is not None:
            # follow times and acceleration of leader, scale with distance
            # ratio
            self.dt0a, self.dtaa, self.dt1a, \
                self.dt0b, self.dtab, self.dt1b = \
                    leader.dt0a, leader.dtaa, leader.dt1a, \
                    leader.dt0b, leader.dtab, leader.dt1b
            self.ai = leader.ai * dist/leader.dist
            return
        
        if dist < 1e-12:
            # no distance, optionally coast
            self.dt0a, self.dtaa, self.dt1a, \
                self.dt0b, self.dtab, self.dt1b = (0.0, ) * 6
            if dt > 0.0 and tcoast % dt > _eps:
                self.dtc = round(tcoast / dt) * dt
            else:
                self.dtc = tcoast
            
            self.jmx = jmx
            self.ai = 0.0
            return
        
        # jerk-limited acceleration, to vmx
        (dt0a, dtaa, dt1a), (dx0, dxa, dx1), ai = \
            JlimMotionProperty(0.0, 0.0, vmx, 0.0, amx, jmx)
        
        # first do the round off for the jerk phase, if applicable and 
        # warn
        if dt > 0.0 and np.abs(dt0a/dt - np.round(dt0a/dt)) > _eps:
            dprint("need to adjust constant jerk phase through jmx/amx ratio")
            jmx = ai / (np.ceil(dt0a/dt)*dt)
            
            # re-calculate
            (dt0a, dtaa, dt1a), (dx0, dxa, dx1), ai = \
            JlimMotionProperty(0.0, 0.0, vmx, 0.0, amx, jmx)
            
        if dt > 0.0 and np.abs(dtaa/dt - np.round(dtaa/dt)) > _eps:
            dprint("need to adjust constant acceleration phase through jmx/amx/vmx")
            amx = (vmx - dt0a*jmx) / (np.ceil(dtaa/dt)*dt)
        
            # re-calculate
            (dt0a, dtaa, dt1a), (dx0, dxa, dx1), ai = \
            JlimMotionProperty(0.0, 0.0, vmx, 0.0, amx, jmx)
        
        # store the times
        self.dt0a, self.dtaa, self.dt1a = dt0a, dtaa, dt1a

        # distance traveled in acceleration phase
        dacc = dx0 + dxa + dx1

        # braking
        (self.dt0b, self.dtab, self.dt1b), (dx0, dxa, dx1), ai = \
            JlimMotionProperty(vmx, 0.0, 0.0, 0.0, amx, jmx)
        ddec = dx0 + dxa + dx1
        
        # coasting
        if dist - dacc - ddec > _eps:
            dtc = (dist - 2*dacc - ddec)
            
            # need to adjust again?
            if dt > 0.0 and np.abs(dtc/dt - np.round(dtc/dt)) > _eps:
                dprint("need to adjust accel phase for coasting speed")
                frac = dtc/(np.ceil(dtc/dt)*dt)
                ai = frac*ai
                jmx = frac*jmx
                self.dtc = np.ceil(dtc/dt)*dt
            else:
                self.dtc = dtc

        elif dist - dacc - ddec > -_eps:
            dtc = 0.0
        else:
            raise ValueError("lower the max speed, trajectory too large")

        self.jmx = jmx
        self.ai = ai
            
    def dt(self):
        return self.dt0a + self.dtaa + self.dt1a + \
            self.dt0b + self.dtab + self.dt1b + self.dtc
            
    def dist(self):
        return JlimMotionSolution(
            self.dt0a, self.dtaa, self.dt1a, 
            0.0, 0.0, 0.0, 0.0, self.ai, self.jmx)[1]
                        
    def createMotions(self):
        
        # tree with time (float)/motion data
        motions = OOBTree()
        
        # jerk limited acceleration
        if self.dt0a > 0.0:
            motions[0.0] = Motion(0.0, self.ai / self.dt0a)
        if self.dtaa > 0.0:
            # constant acceleration
            motions[self.dt0a] = Motion(self.ai, 0.0)
        # jerk limited decel to 0
        if self.dt0b > 0.0:
            motions[self.dt0a + self.dtaa] = \
                Motion(self.ai, -self.ai / self.dt0b)
        
        if self.dtc > 0.0:
            # add coasting (at max velocity)
            motions[self.dt0a + self.dtaa + self.dt0b] = \
                Motion(0.0, 0.0)
        
        # jerk limited deceleration
        if self.dt1a > 0.0:
            motions[self.dt0a + self.dtaa + self.dt0b + self.dtc] = \
                Motion(0.0, -(self.ai / self.dt1a))
        if self.dtab > 0:
            motions[self.dt0a + self.dtaa + self.dt0b + self.dtc +
                    self.dt1a] = \
                Motion(-self.ai, 0.0)
        if self.dt1b > 0.0:
            motions[self.dt0a + self.dtaa + self.dt0b + self.dtc +
                    self.dt1a + self.dtab] = \
                Motion(-self.ai, self.ai / self.dt1b)
        return motions    
    
class Still:
    
    def __init__(self, dt):
        """
        Keep the simulator at the current position for some time

        Parameters
        ----------
        dt : float
            Duration of the continous position.

        Returns
        -------
        None.

        """
        self.dt = dt
        
    def duration(self):
        return self.dt
    
    def accel(self, t):
        return np.zeros((6, ))

    def derivative(self, x, t):
        return np.zeros((13, ))
 

class Movement:
    
    def __init__(self, x0: Pose, x1: Pose, vmx, amx, jmx, rfactor, dt=0.0):
        """
        velocity, acceleration and jerk-limited movement from x0 to x1

        Parameters
        ----------
        x0 : Pose
            Initial pose for the simulator, position and angles.
        x1 : Pose
            Final pose.
        vmx : float
            Maximum velocity during the movement.
        amx : float
            Maximum acceleration during the movement.
        jmx : float
            Maximum jerk.
        rfactor : float
            Ratio between maximum v, a, j in rotation.
        dt : float
            Minimum movement time.

        Returns
        -------
        None.

        """
                
        # calculate the linear and angle distance between the two poses        
        move = x1 - x0
        dl = np.linalg.norm(move.xyz())
        dr = move.angle()
        dprint("linear", dl, "angle", dr)
        
        # collect motions here
        self.motions = OOBTree()

        # linear and or rotational motion
        self.axis = np.zeros((6,))
        if dl > 1e-12:
            self.axis[:3] = move.xyz() / dl
        if dr > 1e-12:
            self.axis[3:] = move.axis() * rfactor
        elif dr < -1e-12:
            self.axis[3:] = -move.axis() * rfactor
        
        # time needed movement linear/rotation
        lmove = JLimMotionParts(dl, vmx, amx, jmx, dt=dt)
        rmove = JLimMotionParts(dr, vmx*rfactor, amx*rfactor, jmx*rfactor,
                                dt=dt)
        
        if lmove.dt() > rmove.dt():
            # linear movement leading, further scale the axis
            #rmove = JLimMotionParts(dr, leader=lmove)
            self.axis[3:] = dr / dl * self.axis[3:]
            self.dt = lmove.dt()
            self.motions = lmove.createMotions()
            
        else:
            # rotation leading
            self.axis[:3] = dl / dr * self.axis[:3]
            self.dt = rmove.dt()
            self.motions = rmove.createMotions()
            #lmove = JLimMotionParts(dl, leader=rmove)
        
    def duration(self):
        return self.dt

    def accel(self, t):
        
        # where does this segment start
        it = self.motions.maxKey(t)
        
        return self.motions[it].accel(t - it)*self.axis

    def derivative(self, x, t):
        
        res = np.zeros((13, ))
        res[:6] = self.accel(t)
        res[6:9] = x[:3]
        res[9] = -0.5 * np.inner(x[3:6], x[10:])
        res[10:] = 0.5 * x[9]*x[3:6] - np.cross(x[3:6], x[10:])
        return res


class MovementSeries:

    def __init__(self, moves):
        t = 0.0
        self.msets = OOBTree()
        for m in moves:
            print("Move of type", m.__class__.__name__, 
                  "duration", m.duration(), "starting", t)
            self.msets.insert(t, m)
            t += m.duration()
            
        self.dt = t

    def __call__(self, t, x):
        # find the matching motion
        try:
            tm = self.msets.maxKey(t)
        except ValueError as e:
            print(f"no key looking for time {t} in {self.msets}")
            raise e

        #print(f"called for {t} with {x}, found {tm}, returning "
        #      f"{self.msets[tm].derivative(x, t - tm)}")
            
        # get the derivative
        return self.msets[tm].derivative(x, t - tm)
    
    def duration(self):
        return self.dt
        
    def accel(self, t):
        # find the matching motion
        try:
            tm = self.msets.maxKey(t)
        except ValueError as e:
            print(f"no key looking for time {t} in {self.msets}")
            raise e
        return self.msets[tm].accel(t - tm)
        
        
if __name__ == '__main__':
    
    plt.close('all')

    # linear forward
    start = Pose(np.zeros((3,)), [1, 0, 0, 0])
    then = Pose(np.ones((3,)), [0.0, 0.0, 0.2])
    mv = Movement(start, then, 0.5, 2.0, 10.0, 0.5, 0.01)
    st = Still(1.1)
    mv2 = Movement(then, start, 0.5, 2.0, 10.0, 0.5, 0.01)
    # movement series function
    fn = MovementSeries([mv, st, mv2])
    
    
    
    # integrate 
    from scipy.integrate import RK45
    
    dt = 0.01
    tr = np.arange(0, fn.duration(), dt)
    X = np.zeros((tr.size, 13))
    X[0] = start.x
    for i in range(len(tr)-1):
        res = RK45(fn, tr[i], X[i], tr[i+1], rtol=1e-12, atol=1e-16)
        res.step()
        X[i+1] = res.y
        #    print(res)
    
    plt.figure()
    plt.subplot(2,1,1)
    plt.plot(tr, X[:,0])
    plt.subplot(2,1,2)
    plt.plot(tr, X[:,6])
    plt.show()
    
    plt.figure()
    dt = 0.01
    tr = np.arange(0, fn.duration(), dt)
    acc = np.zeros((tr.size, 6))
    for i, t in enumerate(tr):
        acc[i] = fn.accel(t)
    for i in range(6):
        plt.subplot(6,1,i+1)
        plt.plot(tr, acc[:,i])
    
    
    
    plt.show()

    print("end position", X[6:9,-1])
    
    # limits
    vmx = 0.2
    amx = 5
    jmx = 50.0
    
   
    
    
    