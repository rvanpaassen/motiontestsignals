#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 11:52:39 2022

@author: repa
"""

from sympy import Symbol, sin, cos, pi, lambdify, diff
import numpy as np
import quaternion
import itertools
from matplotlib import pyplot as plt
from scipy.integrate import cumtrapz
import json

def getFadeInFadeOut():
    """
    Create functions that calculates a sine, its derivative and
    second derivative, with a cosine fade-in

    Returns
    -------
    fsiny : function
        Calculate a sine with fade in.
    fsindy : function
        Calculate the derivative of the sine during fade in.
    fsinddy : function
        Calculates second derivative of the sine during fade in.
    fsinyo : function
        Calculate a sine with fade out.
    fsindyo : function
        Calculate the derivative of the sine during fade out.
    fsinddyo : function
        Calculates second derivative of the sine during fade out.

    """

    # Sympy symbols
    Tfade = Symbol('T_{fade}')
    Ttotal = Symbol('T_{total}')
    phi0 = Symbol('\phi_0')
    omg = Symbol('\omega')
    t = Symbol('t')

    # basic sine signal with frequency omg, fade-in pertiod over a time Tfade,
    # the initial phase for the fading cosine, Pfade, is manipulated to create
    # either a fade-in (Pfade = 0, t0 = 0) or a fade-out (Pfade manipulated
    # so that pi*t/Tfade+Pfade = pi at the start of fade-out.
    y = sin(omg*t+phi0-omg*Tfade)*(1-cos(pi*t/Tfade))/2

    # create callable functions
    fsiny = lambdify((omg, t, phi0, Tfade), y)
    fsindy = lambdify((omg, t, phi0, Tfade), diff(y,t))
    fsinddy = lambdify((omg, t, phi0, Tfade), diff(diff(y, t), t))

    # fade out
    yo = sin(omg*t+phi0-omg*Tfade)*(1-cos(pi*(t-Ttotal)/Tfade))/2
    fsinyo = lambdify((omg, t, phi0, Tfade, Ttotal), yo)
    fsindyo = lambdify((omg, t, phi0, Tfade, Ttotal), diff(yo,t))
    fsinddyo = lambdify((omg, t, phi0, Tfade, Ttotal), diff(diff(yo, t), t))

    return fsiny, fsindy, fsinddy, fsinyo, fsindyo, fsinddyo


def getConstantSines():
    """Create functions that calculate a sine, its derivative and 
    double derivative.
    """

    # sympy symbols used here
    phi0 = Symbol('\phi_0')
    omg = Symbol('\omega')
    t = Symbol('t')
    Tfade = Symbol('T_{fade}')

    # base function
    y = sin(omg*t+phi0-omg*Tfade)

    # calculate and return lambdified functions for base, and derivatives
    fsiny = lambdify((omg, t, phi0, Tfade), y)
    fsindy = lambdify((omg, t, phi0, Tfade), diff(y,t))
    fsinddy = lambdify((omg, t, phi0, Tfade), diff(diff(y,t), t))
    return fsiny, fsindy, fsinddy


def checkHarmonics(waves, n):
    """
    Verify if a sine set given as multiplication of base frequency has
    overlapping n-th harmonics. Prints out a warning, and returns the
    number of overlaps.

    Parameters
    ----------
    waves : tuple/list of int
        sine frequency, expressed in base frequency.
    n : int
        Nth (>=2) harmonics to check.

    Returns
    -------
    int : Nuber of overlapping harmonics

    """
    nintersect = 0
    w = dict([
        (w, set(np.arange(2,n+1) * w)) for w in waves])
    for wi, wj in itertools.combinations(waves, 2):
        if w[wi].intersection(w[wj]):
            print(f"Common harmonics between base waves {wi} and {wj}: {w[wi].intersection(w[wj])}")
            nintersect += 1
    return nintersect


def sineAmplitude(omega, xmax, xdmax, xddmax):
    """
    Return array of position amplitudes for a range of frequencies, 
    corresponding limits given in position, velocity or acceleration.

    Parameters
    ----------
    omega : iterable of number
        Frequency vector, in rad/s.
    xmax : number
        Maximum amplitude.
    xdmax : number
        Maximum speed.
    xddmax : number
        Maximum acceleration.

    Returns
    -------
    iterable of number
        Maximum amplitude vector, corresponding to omega, obeying the
        limits of x, xd, xdd.

    """
    return np.amin(np.array(
        [omega**0*xmax, omega**(-1)*xdmax, omega**(-2)*xddmax]),axis=0)


def fadedSines(Tfade: float, Ttotal: float, omg: np.array,
               phi0:  np.array, dt: float):
    """
    Create a sine-shaped test signal, with a cosine-shaped fade-in

    Parameters
    ----------
    Tfade : float
        Fade-in/fade-out time.
    Ttotal : float
        Total signal time.
    omg : float or np.array[float]
        Single frequency, or column vector of frequencies.
    phi0 : float
        Single sine starting angle, or column vector of starting angles.
    dt : float
        Time step.

    Returns
    -------
    tuple( np.array(dtype=float), np.array, np.array, np.array)
        Tuple with:
            - row array, time vector
            - array with row, or multiple rows, of sine signals with fade-in.
            - same for velocity
            - same for acceleration

    """
    fsinyi, fsindyi, fsinddyi, fsinyo, fsindyo, fsinddyo = getFadeInFadeOut()
    fsiny, fsindy, fsinddy = getConstantSines()

    # time vector until fade, body and fade-out
    t0 = np.arange(0, Tfade, dt)
    tb = np.arange(Tfade, Ttotal-Tfade, dt)
    t1 = np.arange(Ttotal-Tfade, Ttotal, dt)

    # signal and derivatives in fade-in period
    y0 = fsinyi(omg, t0, phi0, Tfade)
    dy0 = fsindyi(omg, t0, phi0, Tfade)
    ddy0 = fsinddyi(omg, t0, phi0, Tfade)

    # signal during constant amplitude
    yb = fsiny(omg, tb, phi0, Tfade)
    dyb = fsindy(omg, tb, phi0, Tfade)
    ddyb = fsinddy(omg, tb, phi0, Tfade)

    # signal and derivatives during fade-out
    y1 = fsinyo(omg, t1, phi0, Tfade, Ttotal)
    dy1 = fsindyo(omg, t1, phi0, Tfade, Ttotal)
    ddy1 = fsinddyo(omg, t1, phi0, Tfade, Ttotal)

    # produce the results
    return (np.hstack((t0, tb, t1)),
        np.hstack((y0, yb, y1)),
        np.hstack((dy0, dyb, dy1)),
        np.hstack((ddy0, ddyb, ddy1)))

def JlimMotionSolution(dt0: float, dta: float, dt1: float,
                       v0: float, a0: float, v1: float, a1: float,
                       ai: float, j_max: float):
    """
    Return motion phase times and distances for a jerk-lmited motion.

    Parameters
    ----------
    dt0 : float
        First phase, jerk-limited change from acceleration a0 to ai
    dta : float
        Second phase, constant acceleration ai
    dt1 : float
        Third phase, jerk-limited change from acceleration ai to a1
    v0 : float
        Initial velocity for the motion
    a0 : float
        Initial acceleration
    v1 : float
        Final velocity
    a1 : float
        Final acceleration
    ai : float
        Constant acceleration level middle segment
    j_max : float
        Maximum/minimum jerk

    Returns
    -------
    Two tuples and a value, first with (dt0, dta, dt1), i.e., a copy, second tuple
    (dx0, dxa, dx1), distance travelled in initial segment, distance in
    constant acceleration segment and distance in final segment, finally the

    """
    dv0 = 0.5*(a0 + ai)*dt0
    vi0 = v0+0.5*(a0 + ai)*dt0
    v10 = vi0 + ai*dta
    a10 = a1 - 0.5*(a1 + ai)*dt1
    # dv1 = 0.5*(ai + a1)*dt1
    #return ((dt0, dta, dt1),
    ##        (v0*dt0+0.5*a0*dt0**2+1/6*(ai-a0)*dt0**2,
    #         (v0+dv0)*dta+0.5*ai*dta**2,
    #         v1*dt1+0.5*a1*dt1**2+1/6*(a1-ai)*dt1**2), ai)
    return ((dt0, dta, dt1),
            (v0*dt0+0.5*a0*dt0**2+1/6*(ai-a0)*dt0**2,
             vi0*dta+0.5*ai*dta**2,
             v10*dt1+0.5*ai*dt1**2+1/6*(a1-ai)*dt0**2), ai)

def JlimMotionTimes(v0: float, a0: float, v1: float, a1: float,
                    jmx: float, amx: float):
    """
    Calculate times for jerk phase,

    Parameters
    ----------
    v0 : float
        Initial speed.
    a0 : float
        Initial acceleration.
    v1 : float
        Final speed.
    a1 : float
        Final acceleration.
    jmx : float
        Maximum jerk value.
    amx : float
        Maximum acceleration value.

    Returns
    -------
    dt0 : float
        Time for jerk from initial speed/acceleration to intermediate
        acceleration.
    float or None
        Time for period at max acceleration. If None, max acceleration
        was not reached.
    dt1 : float
        Time for jerk from max acceleration to final acceleration.

    """

    dt0 = (amx - a0) / jmx    # time accelerating to a_max
    dt1 = (amx - a1) / jmx    # time decelerating back to a1
    dv0 = 0.5*(a0 + amx)*dt0    # velocity change over first period
    dv1 = 0.5*(a1 + amx)*dt1    # velocity change over last period
    dva = v1-v0-dv0-dv1           # velocity change over constant acc period

    if dva * (v1 - v0) > -1e-12:
        return dt0, max(0.0, dva/amx), dt1
    # indication that max accel not reached
    return dt0, None, dt1

def JlimSolveAintermediate(v0: float, a0: float,
                           v1: float, a1: float, j_max: float):
    """
    Given a transition between initial speed and acceleration, final
    speed and acceleration, and a maximum absolute jerk value, calculate
    the intermediate acceleration values.

    Parameters
    ----------
    v0 : float
        Start speed.
    a0 : float
        Start acceleration.
    v1 : float
        End speed.
    a1 : float
        End acceleration.
    j_max : float
        Maximum jerk value.

    Returns
    -------
    list
        Intermediate acceleration values.

    """

    det = [2*(v1-v0)*j_max + a1**2 + a0**2, -2*(v1-v0)*j_max + a1**2 + a0**2]
    return [ 0.5*np.sqrt(2*d) for d in det if d >= 0]


def JlimMotionProperty(v0: float, a0: float, v1: float, a1: float,
                       a_max: float=15.0, j_max: float=150.0):
    """
    Calculate time and distance span for a given speed/velocity
    transition, with a limit on acceleration and jerk.

    Parameters
    ----------
    v0 : float
        Initial velocity.
    a0 : float
        Initial acceleration.
    v1 : float
        Final velocity.
    a1 : float
        Final acceleration.
    a_max : float, optional
        Maximum acceleration value. The default is 8.
    j_max : float, optional
        Maximum jerk value. The default is 800.0.

    Raises
    ------
    ValueError
        If there is no solution found

    Returns
    -------
    Tuple with time step, maximum acceleration and distance travelled.

    """
    if v1 > v0:

        # consider acceleration a_0 -> a_max -> a_1
        dt0, dta, dt1 = JlimMotionTimes(v0, a0, v1, a1, j_max, a_max)
        if not (dta is None):
            return JlimMotionSolution(dt0, dta, dt1,
                                   v0, a0, v1, a1, a_max, j_max)

        all_ai = JlimSolveAintermediate(v0, a0, v1, a1, j_max)
        for ai in all_ai:
            dt0, dta, dt1 = JlimMotionTimes(v0, a0, v1, a1, j_max, float(ai))
            if not (dta is None) and dta >= 0.0:
                return JlimMotionSolution(
                    dt0, dta, dt1, v0, a0, v1, a1, ai, j_max)
        raise ValueError("No solution")

    else:
        # consider acceleration a_0 -> a_max -> a_1
        dt0, dta, dt1 = JlimMotionTimes(v0, a0, v1, a1, -j_max, -a_max)
        if not (dta is None):
            return JlimMotionSolution(dt0, dta, dt1,
                                   v0, a0, v1, a1, -a_max, -j_max)

        all_ai = JlimSolveAintermediate(v0, a0, v1, a1, -j_max)
        for ai in all_ai:
            dt0, dta, dt1 = JlimMotionTimes(v0, a0, v1, a1, -j_max, -float(ai))
            if not (dta is None) and dta >= 0.0:
                return JlimMotionSolution(
                    dt0, dta, dt1, v0, a0, v1, a1, -ai, -j_max)
        raise ValueError("No solution")



def JlimTransition(x0: float, x1: float,
                   vmx: float, amx: float, jmx: float):
    """
    Jerk-limited transition from one position to a next position

    Parameters
    ----------
    x0 : float
        Initial position.
    x1 : float
        Final position.
    vmx : float
        Maximum speed.
    amx : float
        Maximum acceleration.
    jmx : float
        Maximum jerk.

    Raises
    ------
    ValueError
        When transition with given values is not possible.

    Returns
    -------
    motions : TYPE
        DESCRIPTION.
    n : TYPE
        DESCRIPTION.
    x0 : TYPE
        DESCRIPTION.

    """
    motions = []
    n = x1 - x0
    dist = np.linalg.norm(n)
    n = n / dist

    # acceleration
    (dt0, dta, dt1), (dx0, dxa, dx1), ai = JlimMotionProperty(
        0.0, 0.0, vmx, 0.0, amx, jmx)
    dacc = dx0 + dxa + dx1
    motions.append(((dt0, dta, dt1), ai))

    # calculate the deceleration, to know the trajectory length
    (dt0, dta, dt1), (dx0, dxa, dx1), ai = JlimMotionProperty(
        vmx, 0.0, 0.0, 0.0, amx, jmx)
    ddec = dx0 + dxa + dx1

    if dacc + ddec > dist:
        raise ValueError(
            "cannot make transition with this acceleration and jerk")

    # add coasting
    if dist - dacc - ddec > 1e-12:
        dtc = (dist - dacc - ddec)/vmx
        motions.append(((0.0, dtc, 0.0), 0.0))

    # add deceleration
    motions.append(((dt0, dta, dt1), ai))

    return motions, n, x0

def MotionRest(pos, time):
    return [((0.0, time, 0.0), 0.0)], np.zeros((3,)), pos

def motions_interpolate(motions: list, dt: float):
    """
    Interpolate/calculate drive data for motions defined
    by constant-jerk and constant acceleration sections given in a list
    of motions. 
    
    Motion times for the different phases are rounded to the nearest dt.

    Parameters
    ----------
    motions : list
        Each motion entry consists of the following tuple structure:
            ( [ ((dt0, dta, dt1), ai), ... ], n, x0 )
        - dt0 is the jerk phase from zero acceleration to ai
        - dta is the time at constant acceleration ai
        - dt1 is the constant jerk phase from ai down to 0
        - ai is the intermediate acceleration level 
        - n is a unit vector giving the direction of motion
        - x0 is the initial state/position
    dt : float
        Time step.

    Returns
    -------
    
        Tuple with position, speed and acceleration arrays.

    """
    xyz = []
    uvw = []
    acc = []
    counts = []
    for m, n, x0 in motions:
        parts = []
        for part in m:
            (dt0, dta, dt1), ai = part
            n0 = int(np.round(dt0/dt))
            na = int(np.round(dta/dt))
            n1 = int(np.round(dt1/dt))
            
            
            if abs(na * dt - dta) > 1e-8 and ai != 0.0:
                aic = dta / (na * dt) * ai
                print(
                    f"Acc spread, duration target {dta}, adjusted {na*dt},"
                    f" level target {ai}, adjusted {aic}")
            else:
                aic = ai
                
            # for analysis, record the motion duration and ai
            counts.append(( (n0, na, n1), aic))

            if n0:
                dai = aic/n0
                parts.append(np.arange(0.5*dai, aic, dai))
            if na:
                parts.append(np.ones((na,))*aic)
            if n1:
                dai = aic/n1
                parts.append(np.arange(aic-0.5*dai, 0, -dai))    # down jerk slope
         
        # total acceleration profile for this movement
        a = np.hstack(parts)
        v = cumtrapz(a, dx=dt)
        x = cumtrapz(v, dx=dt)
        xyz.append(x0.tolist())
        xyz.extend((x0 + x.reshape((-1,1)) * n).tolist())
        xyz.append((x0 + x[-1]*n).tolist())
        uvw.append([0,0,0])
        uvw.extend((v.reshape((-1,1)) * n).tolist())
        acc.extend((a.reshape((-1,1)) * n).tolist())
        
    return np.array(xyz), np.array(uvw), np.array(acc), counts

class Pose:
    def __init__(self, *args, **kwargs):
        if len(args) >= 1 and len[args[0]] == 3:
            self.xyz = args[0]
            if len(args) >= 2:
                if len(args[1]) == 3:
                    # euler angles
                    self.quat = np.from_euler_angles(args[1])
                elif len(args[1]) == 4:
                    self.quat = np.as_rotation_vector(args[1])
            else:
                self.quat = np.quaternion(args[1])
                
if __name__ == '__main__':

    # test section
    xyz = np.zeros(3)
    nz = np.array((0, 0, 1.0))
    vmx = 0.2
    amx = 5
    jmx = 50.0
    speeds = (0.1, 0.4, 0.7)
    motions = []

    # move to initial position
    A = 0.4
    motions.append(JlimTransition(xyz, xyz + A*nz, vmx, amx, jmx))
    
    # vertical motions at different speeds
    for vmx in speeds:
        motions.append(JlimTransition(xyz+A*nz, xyz-A*nz, vmx, amx, jmx))
        motions.append(JlimTransition(xyz-A*nz, xyz+A*nz, vmx, amx, jmx))
            
    # move to forward position
    nx = np.array((1.0, 0.0, 0.0))
    vmx = 0.2
    motions.append(JlimTransition(xyz+A*nz, xyz+A*nx, vmx, amx, jmx))
    
    for vmx in speeds:
        motions.append(JlimTransition(xyz+A*nx, xyz-A*nx, vmx, amx, jmx))
        motions.append(JlimTransition(xyz-A*nx, xyz+A*nx, vmx, amx, jmx))
                            
    # and sideways
    ny = np.array((0.0, 1.0, 0.0))
    vmx = 0.2
    motions.append(JlimTransition(xyz+A*nx, xyz+A*ny, vmx, amx, jmx))
    
    for vmx in speeds:
        motions.append(JlimTransition(xyz+A*ny, xyz-A*ny, vmx, amx, jmx))
        motions.append(JlimTransition(xyz-A*ny, xyz+A*ny, vmx, amx, jmx))
    
    # back to the middle
    vmx = 0.2
    motions.append(JlimTransition(xyz+A*ny, xyz, vmx, amx, jmx))

    print("Number of motions", len(motions))
    dt = 0.0005
    xyz, uvw, acc, moves = motions_interpolate(motions, dt)
    t = np.arange(0, xyz.shape[0]*dt, dt)
    
    # as a by-product, save the moves file
    with open('pmdmoves.json', 'w') as mf:
        json.dump({'speeds' : speeds, 
                   'moves': moves,
                   'dofs': ('Z', 'X', 'Y'),
                   'dt': dt,
                   'dts': dt*20}, mf)
 
    plt.figure()
    plt.subplot(3,1,1)
    plt.plot(t, acc[:,0])
    plt.subplot(3,1,2)
    plt.plot(t, acc[:,1])
    plt.subplot(3,1,3)
    plt.plot(t, acc[:,2])
    plt.title("Acceleration")
    
    plt.figure()
    plt.subplot(3,1,1)
    plt.plot(t, xyz[:,0])
    plt.subplot(3,1,2)
    plt.plot(t, xyz[:,1])
    plt.subplot(3,1,3)
    plt.plot(t, xyz[:,2])
    
    print(f"position max x: {np.max(xyz[:,0])} y: {np.max(xyz[:,1])} z: {np.max(xyz[:,2])}")
    print(f"position min x: {np.min(xyz[:,0])} y: {np.min(xyz[:,1])} z: {np.min(xyz[:,2])}")

    # reversal bump analysis
    xyz = np.zeros(3)
    nz = np.array((0, 0, 1.0))
    vmx = 0.2
    amx = 5
    jmx = 50.0
    accs = (0.1, 0.05, 0.02, 0.01, 0.005)
    last_v = 1 * accs[0]
    motions = []
    motionelts = []
    
    # speed transition from zero to positive velocity, and first acceleration level
    speeds = []
    (dt0, dta, dt1), (dx0, dxa, dx1), ai = JlimMotionProperty(
        0.0, 0.0, last_v, 0.0, accs[0], jmx)
    dxcum = sum((dx0, dxa, dx1))
    motionelts.append(((dt0, dta, dt1), accs[0]))
    print(f"Acceleration from 0 to {last_v}")
    print(f"Times {dt0}, {dta}, {dt1}")
    print(f"movement {dx0}-{dxa}-{dx1} to {dxcum}")

    # reversal bump test, speed transition to negative velocity
    for a in accs:
        v = 0.5*a
        speeds.append(v)
        (dt0, dta, dt1), (dx0, dxa, dx1), ai = JlimMotionProperty(
            last_v, 0.0, -v, 0.0, a, jmx)
        sumdt = sum((dt0, dta, dt1))
        dxcum += sum((dx0, dxa, dx1))
        motionelts.append(((dt0, dta, dt1), ai))
        print(f"Speed change from {last_v} to {-v}, acc {a}")
        print(f"Times {dt0}, {dta}, {dt1}")    
        print(f"movement {dx0} -> {dxa} -> {dx1} to {dxcum}")

        dtc = (last_v - v) / a
        dxcum += -dtc*v
        if dtc:
            motionelts.append(((0.0, dtc, 0.0), 0.0))
            print(f"Coast at {-v} for {dtc}, movement {-dtc*v}, to {dxcum}")

        (dt0, dta, dt1), (dx0, dxa, dx1), ai = JlimMotionProperty(
            -v, 0.0, v, 0.0, a, jmx)
        print(f"Speed change from {-v} to {v}, acc {a}")
        print(f"Times {dt0}, {dta}, {dt1}")    
        print(f"movement {dx0} -> {dxa} -> {dx1} to {dxcum}")

        # sumdt = sumdt + sum((dt0, dta, dt1))
        ## dx = (last_v - v)*sumdt
        dxcum += sum((dx0, dxa, dx1))
        motionelts.append(((dt0, dta, dt1), ai))
        last_v = v
        
        
    # end speed, and back
    (dt0, dta, dt1), (dx0, dxa, dx1), ai = JlimMotionProperty(
            last_v, 0.0, 0.0, 0.0, accs[-1], jmx)
    dxcum += sum((dx0, dxa, dx1))
    motionelts.append(((dt0, dta, dt1), ai))
    
    # combine all motions
    nz = np.array((0, 0, 1.0))
    xyz = np.zeros((3,))
    motions.append((motionelts, nz, xyz))
    
    # return to zero
    motions.append(JlimTransition(xyz + dxcum*nz, xyz, 0.1*vmx, 0.1*amx, jmx))
    
    dt = 0.0005
    xyz, uvw, acc, moves = motions_interpolate(motions, dt)
    t = np.arange(0, xyz.shape[0]*dt, dt)

    # as a by-product, save the moves file
    with open('bumpmoves.json', 'w') as mf:
        json.dump({'accelerations' : accs, 
                   'speeds': speeds,
                   'moves': moves,
                   'dofs': ('Z',),
                   'dt': dt,
                   'dts': dt*20}, mf)

    plt.figure()
    plt.subplot(3,1,1)
    plt.plot(t, acc[:,0])
    plt.subplot(3,1,2)
    plt.plot(t, acc[:,1])
    plt.subplot(3,1,3)
    plt.plot(t, acc[:,2])
    plt.title("Acceleration")
    
    plt.figure()
    plt.subplot(3,1,1)
    plt.plot(t, xyz[:,0])
    plt.subplot(3,1,2)
    plt.plot(t, xyz[:,1])
    plt.subplot(3,1,3)
    plt.plot(t, xyz[:,2])
    
    print(f"position max x: {np.max(xyz[:,0])} y: {np.max(xyz[:,1])} z: {np.max(xyz[:,2])}")
    print(f"position min x: {np.min(xyz[:,0])} y: {np.min(xyz[:,1])} z: {np.min(xyz[:,2])}")


    # sine motion check
    waves_lo = [3, 5, 8, 14]
    omg = (np.array(waves_lo) *2*np.pi/40.96).reshape((-1,1))
    phi0 = (np.array((0.4, 4.0, 1.3, 4.5))).reshape((-1,1))
