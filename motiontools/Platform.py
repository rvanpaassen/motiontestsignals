#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  2 10:50:39 2022

@author: repa
"""
# quaternion and jacobian
# see https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7942377
# skew symmetric matrices
# https://academic.csuohio.edu/richter_h/courses/mce647/mce647_4_hand.pdf

import numpy as np
from quaternion import quaternion, as_rotation_matrix

def skew(x):
    return np.array([[0, -x[2], x[1]],
                     [x[2], 0, -x[0]],
                     [-x[1], x[0], 0]])

def quat_from_er(phi, theta=None, psi=None):
    if theta is None:
        theta = phi[1]
        if psi is None:
            psi = phi[2]
        phi = phi[0]
    cf, sf = np.cos(0.5*phi), np.sin(0.5*phi)
    ct, st = np.cos(0.5*theta), np.sin(0.5*theta)
    cp, sp = np.cos(0.5*psi), np.sin(0.5*psi)
    return quaternion(cf*ct*cp + sf*st*sp,
                      sf*ct*cp - cf*st*sp,
                      cf*st*cp + sf*ct*sp,
                      cf*ct*sp - sf*st*cp)

def er_from_quat(q: quaternion):
    return np.array(
        (np.arctan2(2.0*(q.y*q.z + q.x*q.w), 
                  q.w*q.w-q.x*q.x-q.y*q.y+q.z*q.z),
         np.arcsin(-2.0*(q.x*q.z - q.y*q.w)),
         np.arctan2(2.0*(q.x*q.y + q.w*q.z),
                  q.w*q.w +q.x*q.x - q.y*q.y - q.z*q.z)))

class Platform:
    
    def __init__(self, Jmin, JMax, UG_radius, LG_radius,
                 UG_phi, LG_phi, LG_z):
        
        # lower and upper gimbal geometries
        self.LGgeom = np.zeros((3,6), dtype=np.double)
        self.UGgeom = np.zeros((3,6), dtype=np.double)
        
        # upper gimbal positions, relative to UG center 
        psi0d = UG_phi*0.5
        for i in range(0, 6, 2):
            self.UGgeom[:,i] = (
                np.cos(psi0d) * UG_radius, np.sin(psi0d) * UG_radius, 0.0)
            psi0d += np.pi * 2.0/3.0
        psi0d = -UG_phi*0.5
        for i in range(1, 6, 2):
            psi0d += np.pi * 2.0/3.0
            self.UGgeom[:,i] = (
                np.cos(psi0d) * UG_radius, np.sin(psi0d) * UG_radius, 0.0)

        # lower gimbal positions, relative to LG center
        psi0d = np.pi / 3.0 - LG_phi*0.5
        for i in range(0, 6, 2):
            self.LGgeom[:,i] = (
                np.cos(psi0d) * LG_radius, np.sin(psi0d) * LG_radius, LG_z)
            psi0d += np.pi * 2.0/3.0
        psi0d = np.pi / 3 + LG_phi*0.5
        for i in range(1, 6, 2):
            self.LGgeom[:,i] = (
                np.cos(psi0d) * LG_radius, np.sin(psi0d) * LG_radius, LG_z)
            psi0d += np.pi * 2.0/3.0
        
        # initial neutral vector
        self.xn = np.array((0.0, 0.0, 0.0))
        self.at = np.zeros((6,3,3))
        for i in range(6):
            self.at[i] = skew(np.asarray(self.UGgeom[:,i]))
    
    def setPose(self, xcmd, qcmd):
                
        self.xcmd = xcmd
        self.qcmd = qcmd
        self.UGpos = as_rotation_matrix(qcmd) @ self.UGgeom + \
            (self.xn + xcmd).reshape((3,1)) @ np.ones((1,6))
        self.Lvec = self.UGpos - self.LGgeom
        self.L = np.linalg.norm(self.Lvec, axis=0)
        self.Junit = self.Lvec / (np.ones((3,1)) @ self.L.reshape((1,6)))
        

    def getL(self, q):
        R = as_rotation_matrix(quaternion(*q[3:]))
        return np.linalg.norm(R.T @ self.UGgeom + \
            (self.xn + q[:3]).reshape((3,1)) @ np.ones((1,6)) - \
                self.LGgeom, axis=0)
    
    def getF(self, q):
        R = as_rotation_matrix(quaternion(*q[3:]))
        L = R.T @ self.UGgeom + \
            (self.xn + q[:3]).reshape((3,1)) @ np.ones((1,6)) - \
            self.LGgeom
        return np.linalg.norm(L, axis=0)
    
    def getFq(self, q):
        """
        Restrictions functions derivative
        
        Eq 8, page 3
        DOES NOT WORK YET! What is the "skew matrix related with the joint
        position of the fixed base?"

        G*dr seems to give omega x, y, z; in which coordinates? base/inertial? or 
        relative to ug platform?
        
        if inertial, then need cross product w x R*uggeom to get velocity 
        of upper gimbal points, then inner product this with U vector to get
        component in direction of actuator
        
        is equal to cross product -R*uggeom x omega
        
        Velocity wrt platform

        Returns
        -------
        sensitivity matrix.

        """
        res = np.zeros((6,7))
        R = as_rotation_matrix(quaternion(*q[3:]))
        L = R.T @ self.UGgeom + \
            (self.xn + q[:3]).reshape((3,1)) @ np.ones((1,6)) - \
            self.LGgeom
        Ln = np.linalg.norm(L, axis=0)
        U = L / Ln
        G = np.array([[-q[4], q[3], q[6], -q[5]],
                      [-q[5], -q[6], q[3], q[4]],
                      [-q[6], q[5], -q[4], q[3]]])
        res[:,:3] = U.T
        for i in range(6):
            res[i,3:] = -2*U[:,i].T @ R @ self.at[i] @ G
        return res
        
    def getdFdP(self, q):
        R = as_rotation_matrix(quaternion(*q[3:]))
        L = R.T @ self.UGgeom + \
            (self.xn + q[:3]).reshape((3,1)) @ np.ones((1,6)) - \
            self.LGgeom
        Ln = np.linalg.norm(L, axis=0)
        U = L / Ln
        return U
    
    def cartForcesMoments(self, q, qf):
        R = as_rotation_matrix(quaternion(*q[3:]))
        L = R.T @ self.UGgeom + \
            (self.xn + q[:3]).reshape((3,1)) @ np.ones((1,6)) - \
            self.LGgeom
        Ln = np.linalg.norm(L, axis=0)
        U = L / Ln
        res = np.zeros((6,))
        res[:3] = U @ qf
        for i in range(6):
            res[3:] += self.at[i] @ (U[:,i] * qf[i])
        return res

    def momentPerLeg(self, q, qf):
        R = as_rotation_matrix(quaternion(*q[3:]))
        L = R.T @ self.UGgeom + \
            (self.xn + q[:3]).reshape((3,1)) @ np.ones((1,6)) - \
            self.LGgeom
        Ln = np.linalg.norm(L, axis=0)
        U = L / Ln
        res = np.zeros((3,6))
        for i in range(6):
            res[:,i] += self.at[i] @ (U[:,i] * qf[i])
        return res

    def getPoseFromLength(self, L, q=None, niter=3, eps=1e-8):

        # assume a default pose at rest
        if q is None:
            q = q = np.zeros((7,)); q[3] = 1.0
        
        # iterate over max niter steps
        for it in range(niter):
            Dq = -(self.getF(q) - L) @ np.linalg.pinv(self.getFq(q)).T
            q = q + Dq
            
            # test exit when eps reached
            if eps is not None:
                if np.linalg.norm(L - self.getL(q)) < eps:
                    break
        return q
                        
def testDeltas(q0, Delta=0.01):
    
    # test with an x, y, z, phi, theta, psi change
    for i in range(6):
        Dq = np.zeros((7,))
        q = np.copy(q0)
    
        if i < 3:
            Dq[i] = Delta
            q[i] += Delta
        else:
            ptp = np.zeros((3,))
            ptp[i-3] = Delta
            dr = quat_from_er(ptp)
            
            # new pose
            q1 = dr * quaternion(*q0[3:])
            
            # adapt the test q
            q[3] = q1.w
            q[4:] = q1.vec
            
            # copy the delta
            Dq[4:] = 0
            Dq[3] = dr.w
    
        
        print(f"index {i} increased\n", srs.getFq(q0) @ Dq)
        print("delta", srs.getF(q) - srs.getF(q0))
    

if __name__ == '__main__':
    
    srs = Platform(2.131, 3.281, 1.6, 1.65, 
                   2*np.arcsin(1.0/16.0), 2*np.arcsin(3.0/16.5), 2.39)
    
    x0 = np.zeros((3,))
    q0 = quat_from_er(0.0, 0.0, 0.0)
    srs.setPose(x0, q0)
    
    print("at neutral")
    q0 = np.zeros((7,))
    q0[3] = 1.0
    testDeltas(q0)
    
    print("forward position")
    q0[0] = 0.2
    testDeltas(q0)
    L = srs.getF(q0)
    
    q = np.zeros((7,))
    q[3] = 1.0
    for i in range(3):
        Dq = -(srs.getF(q) - L) @ np.linalg.pinv(srs.getFq(q)).T
        q = q + Dq
        print(q)

    # Wow. Estimation NR seems to work, 3 steps from neutral, need to
    # re-normalize the quat after
    print("rotated phi")
    q0[0] = 0.0
    rb = quat_from_er(0.2, 0.0, 0.0)
    q0[3:] = rb.w, rb.x, rb.y, rb.z
    testDeltas(q0)
    print(q0)
    print(srs.getFq(q0))

    L = srs.getF(q0)
    q = np.zeros((7,))
    q[3] = 1.0
    for i in range(3):
        Dq = -(srs.getF(q) - L) @ np.linalg.pinv(srs.getFq(q)).T
        q = q + Dq
        print(q)
    
    q = np.zeros((7,))
    q[3] = 1.0
    forces = np.zeros((6,))
    forces[0] = 1
    print(srs.cartForcesMoments(q, forces))
    
    forces = np.ones((6,))
    print(srs.momentPerLeg(q, forces))