#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 16:37:36 2022

@author: repa
"""

fname = "/home/repa/TUDelft/research/motionandjerk/" + \
             "motion-run20220506.log.bz2"
fname2 = "/home/repa/TUDelft/research/motionandjerk/" + \
             "motion-run220225.log.bz2"
import numpy as np
import bz2
from matplotlib import pyplot as plt
from scipy.fftpack import fft


def sineAnalysis(d, dts, dofs, waves, 
                 collect=None, plot=False, epszero=1e-5):
    """
    Analyse with sine test input signals

    Parameters
    ----------
    d : np.array
        Data array, directly read from motion logger
    dofs : tuple of strings
        Degrees of freedom to analyse.
    waves : iterable of int
        Frequencies of the test signals.
    collect : None or dict
        Already collected data on different frequencies
    plot : bool
        If true, generate plots
    epszero : float
        Level for detection of input (acceleration) change

    Returns
    -------
    collect.

    """
    
    # prepare for number of analyses
    nwaves = len(waves)
    ndofs = len(dofs)
    
    # prepare selection of data
    Tmeas = 40.96
    npts = int(round(Tmeas / dts))
    pseconds = int(round(1/dts))
    t = np.arange(0, Tmeas, dts)
    xclude = np.full((npts,), True)
    wclude = np.full((npts,), True)
    iclude = np.full((npts,), False)
    for w in waves:
        wclude[0] = False
        wclude[-1] = False
        wclude[w] = False
        wclude[-w] = False
        xclude[w] = False
        xclude[2*w] = False
        xclude[3*w] = False
        xclude[4*w] = False
        xclude[-w] = False
        xclude[-2*w] = False
        xclude[-3*w] = False
        xclude[-4*w] = False
        iclude[w] = True
        iclude[-w] = True
    xclude[0] = False
    
    # collect assembles all frequencies/data
    if collect is None:
        
        # new collection of data
        collect = {}        
        collect['waves'] = waves
        collect['H'] = np.zeros((6,6,nwaves), dtype=np.complex128)
        collect['harmonics'] = np.zeros((6,6,nwaves,3))
        collect['rem'] = np.zeros((6,6,1))
        collect['input'] = np.zeros((6,1))
        iw0 = 0
        nruns = 1
        
    else:
        nruns = collect['rem'].shape[-1]+1
        iw0 = len(collect['waves'])
        nwavest = iw0 + nwaves
        cnew = {}
        cnew['waves'] = np.hstack((collect['waves'], waves))
        cnew['H'] = np.zeros((6,6,nwavest), dtype=np.complex128)
        cnew['harmonics'] = np.zeros((6,6,nwavest,3), dtype=np.float64)
        cnew['rem'] = np.zeros((6,6,nruns))
        cnew['input'] = np.zeros((6,nruns))
        cnew['H'][:,:,:-nwaves] = collect['H']
        cnew['harmonics'][:,:,:-nwaves,:] = collect['harmonics']
        cnew['rem'][:,:,:-1] = collect['rem']
        cnew['input'][:,:-1] = collect['input']
        collect = cnew
    
    for i, dof in enumerate(dofs):
        
        # find the first non-zero in the dd as start point
        idx = np.argwhere(np.abs(d[:,38+12+i]) > epszero)[0][0]
        
        print(f"analysis in {dof}, run {nruns}, first data at {idx}")
        # cut off the 9 second fade in, and one extra second
        # (have 60 s recording, need 40.96)
        idx += 10 * pseconds
        
        # use position ref as input, position result as output
        u = d[idx:idx+npts,38+i]
        y = d[idx:idx+npts,74+i]
        
        # make a plot
        fig = plt.figure()
        plt.plot(t, u, t, y)
        plt.title(f"Input and output signal axis {dof}, sine set {nruns}")
        
        fig = plt.figure()
        isub = 1
        for ix, dof2 in enumerate(dofs):
            if i == ix:
                continue
            ax = fig.add_subplot(
                len(dofs)-1, 1, isub, 
                title=f"cross feed from {dof} into {dof2}, sine set {nruns}")
            y2 = d[idx:idx+npts,74+ix]
            ax.plot(t, y2)
            isub += 1
        fig.tight_layout()    
              
        # fft transform
        U = fft(u) * dts/Tmeas
        Y = fft(y) * dts/Tmeas
        
        # input energy 
        collect['input'][i,-1] = np.sum(np.abs(U[waves,])**2)
        
        # collecting
        for i2 in range(ndofs):
            if i == i2:
                collect['H'][i, i2, iw0:] = Y[waves, ] / U[waves, ]
                for ih in range(2,5):
                    collect['harmonics'][i,i2,iw0:,ih-2] = \
                        np.abs(Y[np.array(waves)*ih,])**2 / \
                        np.abs(U[waves,])**2
                collect['rem'][i,i2,-1] = np.sum(np.abs(Y[wclude])**2)
            else:
                yx = d[idx:idx+npts,74+i2]
                Yx = fft(yx) * dts/Tmeas
                collect['H'][i, i2, iw0:] = Yx[waves, ] / U[waves, ]
                for ih in range(2,5):
                    collect['harmonics'][i,i2,iw0:,ih-2] = \
                        np.abs(Yx[np.array(waves)*ih,])**2 / \
                            np.abs(U[waves,])**2
                collect['rem'][i,i2,-1] = np.sum(np.abs(Yx)**2)
                
    return collect


if __name__ == '__main__':
    with bz2.open(fname, 'r') as f:
        data = np.loadtxt(f)
        
    with bz2.open(fname2) as f:
        data2 = np.loadtxt(f)
        
    
    print("Array size", data.shape)
    
    dtg = 0.0005
    dts = 0.01
    Twaves = 40.96
    pseconds = int(round(1/dts))
    
    plt.close('all')
    
    d2 = data#[np.logical_and(data[:,0]*dtg > 130, data[:,0]*dtg < 150),:]
    
    # corrections
    data[:,76] = -data[:,76] + 2.39    # z axis
    data[:,77] = -data[:,77]           # phi
    data[:,78] = -data[:,78]           # theta
    
    t = np.arange(0, dts*d2.shape[0], dts)
    all_dofs = ('X', 'Y', 'Z', 'phi', 'tht', 'psi')
    
    for i, name in enumerate(all_dofs):
        plt.figure()
        plt.suptitle(f'Overview signals in {name}')
        plt.subplot(2,1,1)
        plt.plot(t, d2[:,38+i])
        plt.plot(t, d2[:,74+i])
        plt.subplot(2,1,2)
        plt.plot(t, d2[:,38+12+i])
        plt.title(name)
    
    
    # room for sine transfer function
    waves = (3, 5, 8, 14), (23, 43, 67, 101, 171), (227, 331, 443, 589)
    nwaves = sum([len(wv) for wv in waves])
    omega = np.zeros(nwaves)
    iw = 0
    for wv in waves:
        omega[iw:iw+len(wv)] = np.array(wv)/Twaves*2*np.pi
        iw += len(wv)
    
    
    rfiles = ((all_dofs, 367*100, sineAnalysis, waves[0]),
              (all_dofs, 730*100, sineAnalysis, waves[1]),
              (all_dofs, 100, sineAnalysis, waves[2]),) 
              #'srs-test-motion-speeds.dat', 'srs-test-motion-bump.dat')
    
    idx = 0
    collect = None
    for dofs, offset, analysis, waves in rfiles:
        
        # cut the data to the assumed start
        collect = analysis(
            d2[offset:], dts, dofs, waves=waves, collect=collect)
    
    # plot sine analysis results together
    H = collect['H']
    HD = collect['harmonics']
    for i, dof in enumerate(all_dofs):
        plt.figure()
        plt.suptitle(f"Frequency response dof {dof}")
        plt.subplot(2,1,1)
        plt.loglog(omega, np.abs(H[i,i,:]))
        plt.subplot(2,1,2)
        plt.semilogx(omega, np.unwrap(np.angle(H[i,i,:]))*180/np.pi)
        
        #for iw, w in enumerate(omega):
        #    print(f"DOF {dof}, at {w:.2f} rad/s HN (%): {HD[i,iw, 0]}, {HD[i,iw, 1]}, {HD[i,iw, 2]}")
        #for iws in range(len(waves)):
        #    print(f"DOF {dof}, total noise series {iws+1}: {HD0[i,iws]}, power: {PW0[i,iws]}")
        
