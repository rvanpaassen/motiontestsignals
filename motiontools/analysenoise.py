#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 21:32:20 2022

@author: repa
"""
import json
import bz2
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft

dof_index = {'X': 0, 'Y': 1, 'Z': 2, 'phi': 3, 'tht': 4, 'psi': 5}


def roughnessAnalysis(d, dts, dt, tskip, dofs, moves, speeds, epszero=1e-5):
    
    factor = int(round(dts/dt))
    imv = 0
    # find the first non-zero data for this dof
    idf = dof_index[dofs[0]]
    idx = np.argwhere(d[:,38+12+idf] > epszero)[0][0]

    nskip = int(round(tskip/dts))

    # work with the pre-sample idx
    idx = idx*factor
    imv = 0

    sqerr = []

    for dof in dofs:
        
        # allow for skipped dofs in this section
        if dof is None:
            continue
        idf = dof_index[dof]

        # skip the positioning movement, an acel, coast, decel
        idx += sum(moves[imv][0]) + sum(moves[imv+1][0]) + \
            sum(moves[imv+2][0])
        imv = 3
        ix = idx // factor
        print(f"offset for {dof}: {ix}")
        
        # resulting squared error(s)
        sqerr.append([])        
        
        fig = plt.figure()
        
        for ispd, spd in enumerate(speeds):
            
            # each speed is given once positive, once negative, and 
            # consists of three movements, accel, coast, decel, analysing
            # only the coast
            ai = moves[imv+1][1]
            # get the definition of the constant speed move
            n0, na, n1 = moves[imv+1][0]
            if ai != 0.0:
                raise ValueError(f"Fault, no coast in move {imv+1}")
            if n0 != 0 or n1 != 0:
                raise ValueError(f"Fault, nose/tail for move {imv+1}")
            
            # get the number of steps in the transition now
            n0 = sum(moves[imv][0])
            n1 = sum(moves[imv+2][0])
            
            # subtract speed, and plot
            ax = fig.add_subplot(
                len(speeds), 2, 2*ispd+1, 
                title=f"response at speed {spd}, along {dof}")
            
            ix = idx // factor
            npt = (n0+na+n1) // factor
            
            t = np.arange(0.0, npt*dts, dts)
            
            ax.plot(t, d[ix:ix+npt,74+idf] - d[ix:ix+npt,38+idf], 'g-',
                    t, d[ix+npt:ix+2*npt,74+idf] - d[ix+npt:ix+2*npt,38+idf], 'b-', 
                    [n0*dt, (n0+na)*dt], [0.0, 0.0], 'r.')
            
            na2 = (na // (factor * 2) ) * 2 
            Tmeas = na2 * dts 
            x = d[ix+n0//factor:ix+n0//factor+na2,74+idf] - \
                d[ix+n0//factor:ix+n0//factor+na2,38+idf]
            X = (fft(x)*dts/Tmeas)[1:na2//2+1]
            w = np.arange(1/Tmeas, 0.5/dts+0.5/Tmeas, 1/Tmeas)
            
            ax = fig.add_subplot(
                len(speeds), 2, 2*ispd+2, 
                title=f"PSD {spd}, {dof}")
            ax.plot(w, np.abs(X)**2, 'b.')
            fig.tight_layout()    
            
            # numerical result error squared
            sqerr[-1].append(
                (np.sum((x-np.sum(x)/na2)**2)/na2, 
                 np.sum((x[nskip:]-np.sum(x[nskip:])/(na2-nskip))**2
                        /(na2-nskip))))
            
            # update the data index
            idx += 2*(n0+na+n1)
            # and consider the next two moves
            imv += 6
            
    return sqerr

if __name__ == '__main__':

    plt.close('all')    

    # open the data file
    fname = "/home/repa/TUDelft/research/motionandjerk/" + \
                 "motion-run20220506.log.bz2"
    with bz2.open(fname, 'r') as f:
        data = np.loadtxt(f)

    # correction for z:
    data[:,74+2] = -data[:,74+2] + 2.39
    data[:,74+3] = -data[:,75+3]
    data[:,74+4] = -data[:,75+4]

    # read the moves definition; gives movement times based on a 2000 Hz 
    # setpoint generation; this is later re-sampled to 100 Hz for SRS
    with open('../notebooks/pmdmoves.json', 'r') as fm:
        mspec = json.load(fm)
        moves = mspec['moves']
        speeds = mspec['speeds']
        dofs = mspec['dofs']
        dts = mspec['dts']
        dt = mspec['dt']
    
    t_pmd = 538.0
    offset = int(round(t_pmd/dts))
    
    fig = plt.figure()
    ax = fig.add_subplot()
    ax.plot(data[offset:,38+2])
    ax.plot(data[offset:,74+2])
    
    sqrerr = roughnessAnalysis(
        data[offset:,:], dts, dt, 0.5, dofs, moves, speeds)   
    
    
    